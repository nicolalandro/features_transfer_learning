DATASET='cifar10'
DATASET_PATH='/home/supreme/datasets/cifar10'
GPU='0'

python3.7 src/learn_transfer.py --simple-model 'resnet18' --random_gen_size 178 --lr 0.01 --stepsize 50 --gamma 0.1 --gpu $GPU --dataset $DATASET  --dataset_path $DATASET_PATH
