# Is One Teacher Model Enough to Transfer Knowledge to a Student Model?

![](documentation_images/human_readable_image/multi_teacher3.png)

This repo is used to do experiments for the paper in title to conduct experiments. The approach add the possibility to transfer the learning from any teacher models to any student models that have a fueature layer, and we can expand it also to many teachers.

![](documentation_images/TranslatorTwoTeacher_horizzontal.drawio.drawio.png)


## Run Experiments

Install requirements
```
pip install -r requirements.txt
```

Create feature folder for models:
* run src/demo/create_features_dataset.py for a generic model
* run src/demo/create_resnet_features_dataset.py
* run src/demo/create_ntsnet_features_dataset.py

Experiments with one teacher with adapted features size:
* run src/demo/features_learn_transfer.py

Experiments with one teacher:
* run src/demo/features_learn_transfer_translator.py

Experiment with two teacher:
* run src/demo/2features_learn_transfer_translator.py

# Citation
```bibtex
@article{landro2021,
  title={Is One Teacher Model Enough to Transfer Knowledge to a Student Model?},
  author={Landro, Nicola and Gallo, Ignazio and La Grassa, Riccardo},
  journal={algorithms},
  year={2021}
}
```
