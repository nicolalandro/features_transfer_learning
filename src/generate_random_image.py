import torch
from torchvision import transforms

tensor_images = torch.rand(1, 3, 32, 32)
transforms.ToPILImage()(tensor_images[0].cpu()).save('test.png')
