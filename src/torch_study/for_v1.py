import torch

points = torch.tensor(
    [
        [1, 2, 5],
        [3, 4, 5],
        [5, 6, 5]
    ]
)

centers = torch.tensor(
    [
        [7, 8, 5],
        [9, 10, 5]
    ]
)

repeat_point = points.repeat(centers.size(0), 1)
print(repeat_point)
repeat_center = centers.repeat(1, points.size(0))
# print(repeat_center)
repeat_center = repeat_center.reshape(repeat_point.size(0), centers.size(1))
print(repeat_center)

