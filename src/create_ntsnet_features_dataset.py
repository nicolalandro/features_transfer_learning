import os

import torch
import torchvision
from PIL import Image
from torchvision import transforms
from tqdm import tqdm

from src.custom_datasets.read_image_and_name import ImageAndNameFolder
from src.custom_models.nts_net.model import ntsnet

use_gpu = torch.cuda.is_available()

batch_size = 20
output_path = 'datasets/CUB200_2011'
dataset_path = '/media/mint/Caddy/Datasets/CUB_200_2011/'
environment = 'test'

transform_test = transforms.Compose([
    transforms.Resize((600, 600), Image.BILINEAR),
    transforms.CenterCrop((448, 448)),
    # transforms.RandomHorizontalFlip(), # solo se train
    transforms.ToTensor(),
    transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
])
testset = ImageAndNameFolder(root=dataset_path + environment, transform=transform_test)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=2)

net = ntsnet('models/nts_net_moreno.ckpt', device='cpu')
# net = nn.DataParallel(net).cuda()

net.eval()

with torch.no_grad():
    for data, img_names in tqdm(testloader):
        concat_out, raw_logits, concat_logits, part_logits, top_n_index, top_n_prob = net(data)

        for img, file_name in zip(concat_out, img_names):
            splitted_filename = file_name.split('/')
            folder_path = os.path.join(output_path, environment, splitted_filename[-2])
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)
            file_output_path = os.path.join(folder_path, splitted_filename[-1] + '.pt')
            torch.save(img, file_output_path)
