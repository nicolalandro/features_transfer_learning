from ptflops import get_model_complexity_info
from torch import nn

from cifar10_models import resnet50, inception_v3
from src.custom_models.simplified_inceptionv3 import inception_simplified
from src.demo.nts_net.model import resnet

num_classes = 131


def create_resnet50(n_class):
    net = resnet.resnet50(pretrained=False)
    net.avgpool = nn.AdaptiveAvgPool2d(1)
    net.fc = nn.Linear(512 * 4, n_class)
    return net


models = {
    'resnet50': resnet50(),
    'inceptionv3': inception_v3(),
    'simply_inceptionv3': inception_simplified.inception_v3()
}

for k, model in models.items():
    params = sum(p.numel() for p in model.parameters())

    for sample in [(3, 32, 32), (3, 224, 224), (3, 448, 448)]:
        try:
            macs, _ = get_model_complexity_info(
                model,
                sample,
                as_strings=False,
                print_per_layer_stat=False,
                verbose=False,
            )
            break
        except:
            pass
    print(k)
    print('\tparams:', f'{params:.3E}')
    print('\tflops:', f'{macs:.3E}')
