import os

import torch
import torchvision
from torchvision.transforms import transforms


class ImageAndFeatureFolder(torchvision.datasets.ImageFolder):
    def __init__(self, root, features_root, transform=None, target_transform=None):
        super(ImageAndFeatureFolder, self).__init__(root,
                                                    transform,
                                                    target_transform)
        self.features_root = features_root

    def __getitem__(self, index):
        path, target = self.samples[index]
        sample = self.loader(path)
        if self.transform is not None:
            sample = self.transform(sample)
        if self.target_transform is not None:
            target = self.target_transform(target)

        splitted_path = path.split('/')
        feature_path = os.path.join(self.features_root, splitted_path[-3], splitted_path[-2], splitted_path[-1] + '.pt')
        feature = torch.load(feature_path)
        return sample, feature, target


class ImageAnd2FeatureFolder(torchvision.datasets.ImageFolder):
    def __init__(self, root, features_root1, features_root2, transform=None, target_transform=None):
        super(ImageAnd2FeatureFolder, self).__init__(root,
                                                     transform,
                                                     target_transform)
        self.features_root1 = features_root1
        self.features_root2 = features_root2

    def __getitem__(self, index):
        path, target = self.samples[index]
        sample = self.loader(path)
        if self.transform is not None:
            sample = self.transform(sample)
        if self.target_transform is not None:
            target = self.target_transform(target)

        splitted_path = path.split('/')

        feature_path1 = os.path.join(self.features_root1, splitted_path[-3], splitted_path[-2],
                                     splitted_path[-1] + '.pt')
        feature1 = torch.load(feature_path1)

        feature_path2 = os.path.join(self.features_root2, splitted_path[-3], splitted_path[-2],
                                     splitted_path[-1] + '.pt')
        feature2 = torch.load(feature_path2)

        return sample, feature1, feature2, target


if __name__ == '__main__':
    batch_size = 1
    path = '/media/mint/Caddy/Datasets/CUB_200_2011/test'
    feature_path = 'datasets/CUB200_2011'

    image_shape = (64, 64)
    data_transform = transforms.Compose([
        transforms.Resize(image_shape),
        transforms.ToTensor(),
        # transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])

    imageset = ImageAndFeatureFolder(root=path, features_root=feature_path, transform=data_transform)
    # print(.__getitem__(0))
    imageloader = torch.utils.data.DataLoader(imageset, batch_size=batch_size, shuffle=False, num_workers=2)
    # print('num classes:', num_classes)

    device = torch.device('cpu')
    for epoch in range(2):
        for batch_idx, (inputs, features, targets) in enumerate(imageloader):
            inputs = inputs.to(device)
            print('train input', inputs.shape)
            print('train features', features.shape)
            # print(epoch, inputs[0][0])
            print('train output', targets.shape)
            exit()
