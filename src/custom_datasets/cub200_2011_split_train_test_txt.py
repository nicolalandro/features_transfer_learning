import os
import cv2

def main():
        




    txt_file = '/home/super/datasets/CUB_200_2011/CUB_200_2011/txt_file/complied_file_class_info.txt'
    txt_file_read = open(txt_file, 'r') 
    all_lines = txt_file_read.readlines()
    
    id_txt_dic = {}

    for sline in all_lines:
        sline = sline.strip()
        sline = sline.split('|')
        identifier = sline[0]
        title = sline[1]
        id_txt_dic[identifier] = title
    

    train_file = open('./txt_file/train_file_class_info_1.txt','w')
    test_file = open('./txt_file/test_file_class_info_1.txt','w')

    readFile = 'images-copy.txt'

    file_to_read = open(readFile, 'r') 
    lines = file_to_read.readlines()

    for line in lines:
        line = line.strip()
        line = line.split(' ')
        num = line[0]
        img_path = line[1]
        split_label = line[2]
        class_label = line[3]

        file_name = img_path.split('/')[-1]
        class_label = img_path.split('/')[0]        




        if split_label == '1':
            fid= file_name.split('.')[0]
            for idx in xrange(1):
                concat_text = '#'+str(idx)
                key = fid + concat_text
                get_txt = id_txt_dic.get(key,"")
                train_file.write(file_name + '#' +str(idx) +  '|' + get_txt + '|' + class_label  + '\n')
        else:
            fid= file_name.split('.')[0]
            for idx in xrange(1):
                concat_text = '#'+str(idx)
                key = fid + concat_text
                get_txt = id_txt_dic.get(key,"")
                test_file.write(file_name + '#' +str(idx) + '|' +get_txt + '|' + class_label  + '\n')
    test_file.close()
    train_file.close()

           


if __name__ == '__main__':
    main()
