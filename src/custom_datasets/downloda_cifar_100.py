import os
import numpy as np
from tqdm import tqdm
import imageio

def unpickle(file):
    import pickle
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict


def mkdir_if_missing(directory):
    if not os.path.isdir(directory):
        os.makedirs(directory)


original_dataset_path = '/home/minimol/Scaricati/cifar100/cifar-100-python'
output_path = '/home/minimol/datasets/cifar100'

mkdir_if_missing(output_path)

meta = unpickle(os.path.join(original_dataset_path, 'meta'))
fine_label_names = [t.decode('utf8') for t in meta[b'fine_label_names']]

environ = 'test'
train_output_path = os.path.join(output_path, environ)
mkdir_if_missing(train_output_path)

train = unpickle(os.path.join(original_dataset_path, environ))

filenames = [t.decode('utf8') for t in train[b'filenames']]
fine_labels = train[b'fine_labels']
data = train[b'data']

images = list()
for d in data:
    image = np.zeros((32, 32, 3), dtype=np.uint8)
    image[..., 0] = np.reshape(d[:1024], (32, 32))  # Red channel
    image[..., 1] = np.reshape(d[1024:2048], (32, 32))  # Green channel
    image[..., 2] = np.reshape(d[2048:], (32, 32))  # Blue channel
    images.append(image)

for index, image in tqdm(enumerate(images)):
    filename = filenames[index]

    label = fine_labels[index]
    label = fine_label_names[label]
    class_dir_folder = os.path.join(train_output_path, label)
    mkdir_if_missing(class_dir_folder)

    imageio.imwrite(os.path.join(class_dir_folder, filename), image)
