import os

import torch
import torchvision
import torchvision.transforms as transforms


def get_dataset_loaders(path, batch_size):
    transform_train = transforms.Compose([
        # transforms.RandomCrop(32, padding=4),
        # transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    ])
    train_path = os.path.join(path, 'train')
    trainset = torchvision.datasets.ImageFolder(root=train_path, transform=transform_train)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=2)

    transform_test = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    ])
    test_path = os.path.join(path, 'test')
    testset = torchvision.datasets.ImageFolder(root=test_path, transform=transform_test)
    testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=2)

    classes_dict = {i: v for i, v in enumerate(trainset.classes)}
    print(classes_dict)

    return trainloader, testloader, len(trainset.classes)


if __name__ == '__main__':
    batch_size = 128
    path = '/home/supreme/datasets/cifar10/'

    trainloader, testloader, num_classes = get_dataset_loaders(path, batch_size)
    print('num classes:', num_classes)

    device = 'cpu'
    for epoch in range(2):
        for batch_idx, (inputs, targets) in enumerate(trainloader):
            inputs, targets = inputs.to(device), targets.to(device)
            print('train', inputs.shape)
            print(epoch, inputs[0][0])
            print(targets.shape, targets[0])
            break

    for batch_idx, (inputs, targets) in enumerate(testloader):
        inputs, targets = inputs.to(device), targets.to(device)
        print('test', inputs.shape)
        break
