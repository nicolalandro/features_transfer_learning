import os
from PIL import Image

import torch
import torchvision
import torchvision.transforms as transforms


class ImageAndNameFolder(torchvision.datasets.ImageFolder):
    def __getitem__(self, index):
        path, _ = self.samples[index]
        sample = self.loader(path)
        if self.transform is not None:
            sample = self.transform(sample)

        return sample, path


def get_dataset_loaders(path, batch_size):
    image_shape = (64, 64)
    data_transform = transforms.Compose([
        transforms.Resize(image_shape),
        transforms.ToTensor(),
        # transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])

    imageset = ImageAndNameFolder(root=path, transform=data_transform)
    # print(.__getitem__(0))
    imageloader = torch.utils.data.DataLoader(imageset, batch_size=batch_size, shuffle=True, num_workers=2)

    return imageloader


if __name__ == '__main__':
    batch_size = 1
    path = '/media/mint/Caddy/Datasets/CUB_200_2011/'

    imageloader = get_dataset_loaders(path, batch_size)
    # print('num classes:', num_classes)

    device = torch.device('cpu')
    for epoch in range(2):
        for batch_idx, (inputs, targets) in enumerate(imageloader):
            inputs = inputs.to(device)
            print('train input', inputs.shape)
            # print(epoch, inputs[0][0])
            print('train output', len(targets), targets)
            exit()
