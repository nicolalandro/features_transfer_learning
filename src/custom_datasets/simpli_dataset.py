import os
import os.path
import random

import PIL.Image
import torch
import torchvision


class MyDatasetMod(object):
    def __init__(self, batch_size, path):
        trainloader = []
        train_labels = []
        testloader = []
        test_labels = []
        self.batch_size = batch_size
        self.path = path

        # train_transforms = torchvision.transforms.Compose([
        #     # torchvision.transforms.Resize(size=(32, 32)),
        #     torchvision.transforms.ToTensor()
        # ])
        #
        # test_transforms = torchvision.transforms.Compose([
        #     # torchvision.transforms.Resize(size=(32,32)),
        #     torchvision.transforms.ToTensor()
        # ])

        train_transforms = torchvision.transforms.Compose([
            # torchvision.transforms.RandomCrop(32, padding=4),
            # torchvision.transforms.RandomHorizontalFlip(),
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
        ])

        test_transforms = torchvision.transforms.Compose([
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
        ])

        folders = (i for i in os.listdir(path + '/train'))
        class_idx = {key: value for (key, value) in enumerate(folders)}
        print("Class list: ", class_idx)

        for index, key in enumerate(range(len(class_idx))):
            images_names = os.listdir(path + '/train' + '/' + class_idx[key])
            for index_en, i in enumerate(images_names):
                print(
                    f'\rTRAIN LOADING class: {(index + 1)}/{len(class_idx)}, image: {(index_en + 1)}/{len(images_names)}',
                    end='')

                if i.endswith(".png") or i.endswith(".JPEG") or i.endswith(".jpg"):
                    try:
                        image = PIL.Image.open(path + '/train' + '/' + class_idx[key] + '/' + i)
                        image_tensor = train_transforms(image)
                        if image_tensor.shape[0] == 3:  # channel
                            trainloader.append(image_tensor)
                            train_labels.append(index)
                    except Exception as e:
                        pass
                    image.close()
        print('\n')

        # TODO for train/test splitted
        for index, key in enumerate(range(len(class_idx))):
            images_names_test = os.listdir(path + '/test' + '/' + class_idx[key])
            for index_img, i in enumerate(images_names_test):
                print(
                    f'\rTEST LOADING class: {(index + 1)}/{len(class_idx)}, image: {(index_img + 1)}/{len(images_names_test)}',
                    end='')

                if i.endswith(".png") or i.endswith(".JPEG") or i.endswith(".jpg"):
                    try:
                        image = PIL.Image.open(path + '/test' + '/' + class_idx[key] + '/' + i)
                        image_tensor = test_transforms(image)
                        if image_tensor.shape[0] == 3:  # channels
                            testloader.append(image_tensor)
                            test_labels.append(index)
                    except Exception as e:
                        pass
                    image.close()
        print('\nStart to splitting')

        c = list(zip(trainloader, train_labels))
        random.shuffle(c)
        train_data, train_labels = zip(*c)

        train_data = torch.stack(train_data)
        train_data, train_labels, = torch.tensor(train_data), torch.tensor(train_labels)

        trainloader = torch.split(train_data, batch_size)
        train_labels = torch.split(train_labels, batch_size)

        c = list(zip(testloader, test_labels))
        random.shuffle(c)
        testloader, test_labels = zip(*c)

        testloader = torch.stack(testloader)
        test_data, test_labels = torch.tensor(testloader), torch.tensor(test_labels)

        testloader = torch.split(test_data, batch_size)
        test_labels = torch.split(test_labels, batch_size)

        self.trainloader = trainloader
        self.train_labels = train_labels
        self.testloader = testloader
        self.test_labels = test_labels
        self.num_classes = len(class_idx)

        print("Full Dataset size: ", len(trainloader), " Dataset percentage used: ", 100, "% ", "Classes: ",
              self.num_classes, "Test loader set: ", len(testloader))
