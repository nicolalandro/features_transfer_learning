import os
from shutil import copy, move

import pandas as pd
import matplotlib.pyplot as plt


def show_bar_plot(species):
    plt.bar(range(len(species)), list(species.values()), align='center')
    plt.xticks(range(0, len(species)), [f'({i}) {s}' for i, s in enumerate(species.keys())])
    plt.xticks(rotation=90)
    plt.show()


if __name__ == '__main__':
    # OXFORD cat and dog
    ox_path = '/media/mint/Caddy/Datasets/LorettaDogs/cats-and-dogs-breeds-classification-oxford-dataset/annotations/annotations/list.txt'
    df = pd.read_csv(ox_path, sep=' ', skiprows=range(0, 6), names=['img', 'id', 'species', 'breed_id'])
    dogs_df = df[df['species'] == 2]
    ox_species = dict()
    for i, row in dogs_df.iterrows():
        name = row['img']
        dog_class = '_'.join(name.split('_')[:-1])
        if dog_class in ox_species.keys():
            ox_species[dog_class] += 1
        else:
            ox_species[dog_class] = 1
    # show_bar_plot(ox_species)

    # stanfordogs
    sf_path = '/media/mint/Caddy/Datasets/LorettaDogs/StanfordDogs/Images'

    sf_species = {s.split('-')[-1].lower(): len(os.listdir(os.path.join(sf_path, s))) for s in os.listdir(sf_path)}
    # show_bar_plot(sf_species)

    # classi in comune
    common = 0
    for k in ox_species.keys():
        if k in list(sf_species.keys()):
            print(k)
            common += 1
    print('common_classes are:', common)

    output = '/media/mint/Caddy/Datasets/LorettaDogs/all_images'
    for s in os.listdir(sf_path):
        cat = s.split('-')[-1].lower()
        output_dir_path = os.path.join(output, cat)
        if not os.path.exists(output_dir_path):
            os.makedirs(output_dir_path)
        category_folder = os.path.join(sf_path, s)
        for f in os.listdir(category_folder):
            src = os.path.join(category_folder, f)
            dest = os.path.join(output_dir_path, f)
            copy(src, dest)

    category_folder = '/media/mint/Caddy/Datasets/LorettaDogs/cats-and-dogs-breeds-classification-oxford-dataset/images/images'
    for i, row in dogs_df.iterrows():
        name = row['img']
        dog_class = '_'.join(name.split('_')[:-1])
        output_dir_path = os.path.join(output, dog_class)
        if not os.path.exists(output_dir_path):
            os.makedirs(output_dir_path)
        f = name + '.jpg'
        src = os.path.join(category_folder, f)
        dest = os.path.join(output_dir_path, f)
        copy(src, dest)
    test = '/media/mint/Caddy/Datasets/LorettaDogs/test'
    for cat in os.listdir(output):
        output_dir_path = os.path.join(test, cat)
        if not os.path.exists(output_dir_path):
            os.makedirs(output_dir_path)
        category_folder = os.path.join(output, cat)
        listdir = os.listdir(category_folder)
        for f in listdir[:int(len(listdir) * 20 / 100)]:
            src = os.path.join(category_folder, f)
            dest = os.path.join(output_dir_path, f)
            move(src, dest)
