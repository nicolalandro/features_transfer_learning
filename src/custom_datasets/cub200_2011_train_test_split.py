import os
import cv2
from tqdm import tqdm


def main():
    images_path = '/home/supreme/datasets/CUB_200_2011/images/images/'
    base_output_path = '/home/supreme/datasets/CUB_200_2011/'

    readFile = 'images-copy.txt'

    file_to_read = open(readFile, 'r')
    lines = file_to_read.readlines()

    pbar = tqdm(lines)
    for line in pbar:
        line = line.strip()
        line = line.split(' ')
        num = line[0]
        img_path = line[1]
        split_label = line[2]
        class_label = line[3]

        file_name = img_path.split('/')[-1]
        class_label = img_path.split('/')[0]

        img_file = images_path + class_label + '/' + file_name

        if split_label == '1':
            if not os.path.exists(base_output_path + 'train' + '/' + class_label):
                os.makedirs(base_output_path + 'train' + '/' + class_label)
            img = cv2.imread(img_file)
            write_path = base_output_path + 'train' + '/' + class_label + '/' + file_name
            cv2.imwrite(write_path, img)

        else:
            if not os.path.exists(base_output_path + 'test' + '/' + class_label):
                os.makedirs(base_output_path + 'test' + '/' + class_label)
            img = cv2.imread(img_file)
            write_path = base_output_path + 'test' + '/' + class_label + '/' + file_name
            cv2.imwrite(write_path, img)
        pbar.set_description(split_label + ' - ' + img_file + ' --> ' + write_path)


if __name__ == '__main__':
    main()
