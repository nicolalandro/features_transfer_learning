import os

import matplotlib.pyplot as plt


def get_value_after(log_path, v):
    with open(log_path, "r") as f:
        logs = str(f.readlines())
    splits_1 = logs.split(v)[1:]
    x = []
    y = []
    for epoch, split in enumerate(splits_1):
        try:
            accuracy = float(split.split('\\n')[0].strip())
        except:
            accuracy = float(split.split(' ')[1].strip())
        x.append(epoch + 1)
        y.append(accuracy)
    return x, y


if __name__ == '__main__':
    base_path = 'logs/translator'
    logs = os.listdir(base_path)

    selection_string = 'Test MSE(reduction sum):'

    for filename in logs:
        log_path = f'{base_path}/{filename}'
        try:
            x, y = get_value_after(log_path, selection_string)
            print(filename, 'mse:', min(y), f'(at epoch {y.index(min(y))})', 'epochs:', len(x))
        except:
            print('ERROR on mse:', log_path)

        try:
            xa, ya = get_value_after(log_path, 'Accuracy(multiclass):')
            print('\t', 'acc:', ya[y.index(min(y))])
            print(filename, 'acc:', max(ya), f'(at epoch {ya.index(max(ya))})', 'epochs:', len(x))
            print('\t', 'mse:', y[ya.index(max(ya))])
        except:
            print('ERROR on acc:', log_path)

        print('\n')
