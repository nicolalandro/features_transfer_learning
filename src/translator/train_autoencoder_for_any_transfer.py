import argparse
import datetime
import os
import time

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
from torch.optim import lr_scheduler

from src.custom_models.simple_autoencoder import SimpleAutoencoder
from src.utils import AverageMeter

parser = argparse.ArgumentParser("Transfer Knowledge from resnet 50")

# dataset
parser.add_argument('--path', type=str, default='/media/mint/Caddy/Datasets/LorettaDogs/nts-features')

# optimization
parser.add_argument('--batch-size', type=int, default=128)
parser.add_argument('--lr', type=float, default=0.001, help="learning rate for model")
parser.add_argument('--stepsize', type=int, default=50)
parser.add_argument('--gamma', type=float, default=0.1, help="learning rate decay")

# model
parser.add_argument('--max-epoch', type=int, default=200)
parser.add_argument('--when_save_model', type=int, default=1, help="save model at epoch x")
parser.add_argument('--save-dir', type=str,
                    default='/media/mint/Caddy/Datasets/LorettaDogs/ModelsFeatureTransfer/ntsTOmobnetv3largeTranslator')

# pythorch seed
parser.add_argument('--seed', type=int, default=1, help='using a fixed seed to be reproducible')

# selected gpus
parser.add_argument('--gpu', type=str, default='0', help='select the gpu or gpus to use')

args = parser.parse_args()


def main():
    torch.manual_seed(args.seed)

    # args.simple_model = 'resnet50_cifar100'
    print('--- START Arg parse ---')
    options = vars(args)
    for k, v in options.items():
        print(k, ':', v)
    print('--- END Arg parse ---')

    test_gpu_settings()

    train_path = os.path.join(args.path, 'train')
    trainset = torchvision.datasets.ImageFolder(root=train_path, loader=torch.load, is_valid_file=lambda x: True)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True, num_workers=2)
    test_path = os.path.join(args.path, 'test')
    testset = torchvision.datasets.ImageFolder(root=test_path, loader=torch.load, is_valid_file=lambda x: True)
    testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=2)
    features_size = 10240
    encoded_dim = 960

    model = SimpleAutoencoder(input_size=features_size, intermediate_size=encoded_dim)
    model = nn.DataParallel(model).cuda()

    criterion_mse = nn.MSELoss().cuda()

    params = list(model.parameters())
    optimizer = torch.optim.Adam(params, args.lr)
    # optimizer = torch.optim.RMSprop(params, lr=args.lr, alpha=0.99, eps=1e-8, momentum=0,
    #                                 centered=False)

    scheduler = lr_scheduler.StepLR(optimizer, step_size=args.stepsize, gamma=args.gamma)

    start_time = time.time()
    for epoch in range(args.max_epoch):
        print("==> Start Epoch {}/{}".format(epoch + 1, args.max_epoch))
        train_model(model, trainloader, criterion_mse, optimizer)

        scheduler.step()

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

        test(model, testloader)

        if (epoch + 1) % args.when_save_model == 0:  # Save trained model
            print("Model trained saveing...")
            torch.save(model,
                       f'{args.save_dir}/simple_autoencoder_resnet50_to_84_{epoch + 1}.pt')
        print("==> End Epoch {}/{}".format(epoch + 1, args.max_epoch))


def train_model(model, trainloader, criterion_mse, optimizer):
    model.train()
    mse_losses = AverageMeter()

    for batch_idx, (data, labels) in enumerate(trainloader):
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        features, predictions = model(data)
        loss_mse = criterion_mse(predictions, data)

        loss = loss_mse

        optimizer.zero_grad()
        loss.backward()

        optimizer.step()

        mse_losses.update(loss_mse.item(), labels.size(0))

        if (batch_idx + 1) % 10 == 0:
            print(
                f"Batch {batch_idx + 1}\t  MSE {mse_losses.val} ({mse_losses.avg})")


def test(model, testloader):
    model.eval()
    total = 0

    test_mse_loss = 0
    with torch.no_grad():
        for data, labels_categorical in testloader:
            labels_categorical = labels_categorical.cuda(non_blocking=True)
            data = data.cuda(non_blocking=True)

            total += labels_categorical.size(0)

            lenet_features, lenet_predictions = model(data)

            test_mse_loss += F.mse_loss(lenet_predictions, data, reduction='sum').item()

    test_mse_loss /= total
    print(
        f'Test MSE(reduction sum): {test_mse_loss}')


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    # sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
    #     args.percentage_used) + '.txt'))
    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        print(f"Learning rate optimizer {args.lr}  Batch-size {args.batch_size}")
    else:
        print("Currently using CPU")
        exit()


if __name__ == '__main__':
    main()
