import pydot
import torch
from torchviz import make_dot

from src.demo.mobilenet.mobilenetv3 import mobilenetv3_large


def summary(model, input_shape, output_file):
    model.eval()
    x = torch.zeros(1, input_shape[0], input_shape[1], input_shape[2], dtype=torch.float, requires_grad=False)
    out = model(x)
    dot = make_dot(out, params=dict(model.named_parameters()))
    graphs = pydot.graph_from_dot_data(str(dot))
    graphs[0].write_svg(output_file)


num_classes = 131
input_shape = [3, 244, 244]
output_file = '../../logs/mobilenetv3_large.svg'

model = mobilenetv3_large(**{'num_classes': num_classes})

summary(model, input_shape, output_file)
