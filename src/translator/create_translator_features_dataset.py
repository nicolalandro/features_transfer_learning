import os

import torch
from torch import nn
from tqdm import tqdm

from src.custom_datasets.read_image_and_name import ImageAndNameFolder

use_gpu = torch.cuda.is_available()

batch_size = 100
output_path = '/media/mint/Caddy/Datasets/LorettaDogs/nts-features-mobilenetv3-translation'
dataset_path = '/media/mint/Caddy/Datasets/LorettaDogs/nts-features'
model_path = '/media/mint/Caddy/Datasets/LorettaDogs/ModelsFeatureTransfer/ntsTOmobnetv3largeTranslator/simple_autoencoder_comp_ntsnet_to_960_199.pt'
environment = 'train'

train_path = os.path.join(dataset_path, environment)
trainset = ImageAndNameFolder(root=train_path, loader=torch.load, is_valid_file=lambda x: True)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=2)

device = 'cuda'
net = torch.load(model_path)
if device == 'cuda':
    net = nn.DataParallel(net).to(device)

net.eval()

with torch.no_grad():
    for data, img_names in tqdm(trainloader):
        if device == 'cuda':
            data = data.to(device)
        f, _, _ = net(data)
        # f, _ = net(data) # if does not have classifiaction

        for img, file_name in zip(f, img_names):
            splitted_filename = file_name.split('/')
            folder_path = os.path.join(output_path, environment, splitted_filename[-2])
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)
            file_output_path = os.path.join(folder_path, splitted_filename[-1])
            torch.save(img.to('cpu'), file_output_path)
