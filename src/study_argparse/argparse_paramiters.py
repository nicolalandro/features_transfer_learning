import argparse

parser = argparse.ArgumentParser("Test")

parser.add_argument('--gpu', type=str, default='1', help='select the gpu or gpus to use')
parser.add_argument('--max-epoch', type=int, default=200)
parser.add_argument('--simple-model', type=str, default='mobilenetV2',
                    choices=['lenet', 'resnet18', 'mobilenetV2', 'alexnet'])

args = parser.parse_args()
options = vars(args)
for k, v in options.items():
    print(k, ':', v)
