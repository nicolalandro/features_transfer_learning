import torch
from ignite.metrics import Accuracy, ConfusionMatrix

num_classes = 3
x = torch.tensor([
    [.9, .1, 0],
    [.7, .2, .1],
    [0, 0, 1],
    [.3, .5, .2]
])
print('original:', x)


def prediction_to_categorical(val):
    return val.argmax(dim=1, keepdim=True).reshape(-1)


def categorical_to_one_hot(val, num_classes):
    return torch.zeros(len(val), num_classes).scatter_(1, val.unsqueeze(1), 1.)


pred_categorical = prediction_to_categorical(x)
print('pred categorical:', pred_categorical)

pred_one_hot = categorical_to_one_hot(pred_categorical, num_classes)
print('pred one hot:', pred_one_hot)

labels_one_hot = torch.tensor([
    [1., 0., 0.],
    [1., 0., 0.],
    [0., 1., 0.],
    [0., 0., 1.]]
)
print('labels one hot:', labels_one_hot)
labels_categorical = prediction_to_categorical(labels_one_hot)
print('labels categorical:', labels_categorical)

accuracy_metric = Accuracy()
accuracy_metric.update((x, labels_categorical))
# se il tipo non è multiclass è un problema
print(f'accuracy({accuracy_metric._type}):', accuracy_metric.compute())

cnf_matrix = ConfusionMatrix(num_classes=num_classes)
cnf_matrix.update((pred_one_hot, labels_categorical))
print('confusion matrix:', cnf_matrix.compute())
