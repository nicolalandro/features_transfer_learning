import torch
from ignite.metrics import Accuracy, ConfusionMatrix

x = torch.tensor([
    [
        [.9, .1, 0],
        [1, 0, 0],
        [0, 0, 1],
        [0, 1, 0]
    ],
    [
        [1, 0, 0],
        [1, 0, 0],
        [0, 0, 1],
        [0, 1, 0]
    ]
])

y = torch.tensor([
    [
        0,
        1,
        2,
        1
    ],
    [
        0,
        0,
        2,
        1
    ]
])

num_classes = 3

accuracy_metric = Accuracy()
cnf_matrix = ConfusionMatrix(num_classes=num_classes)
cnf_matrix_precision = ConfusionMatrix(num_classes=num_classes, average='precision')
for pred, categorical_labels in zip(x, y):
    labels_one_hot = torch.zeros(len(categorical_labels), num_classes).scatter_(1, categorical_labels.unsqueeze(1), 1.)

    categorical_pred = pred.argmax(dim=1, keepdim=True).reshape(-1)
    pred_one_hot = torch.zeros(len(categorical_pred), num_classes).scatter_(1, categorical_pred.unsqueeze(1), 1.)

    accuracy_metric.update((pred_one_hot, labels_one_hot))
    cnf_matrix.update((pred_one_hot, categorical_labels))
    cnf_matrix_precision.update((pred_one_hot, categorical_labels))

print('accuracy:', accuracy_metric.compute())
print('confusion matrix:', cnf_matrix.compute())
print('confuzion matrix (average precision):', cnf_matrix_precision.compute())
