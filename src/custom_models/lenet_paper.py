import torch
import torch.nn as nn
import torch.nn.functional as F


class LeNet(nn.Module):
    def __init__(self, num_classes, features_size=64):
        super(LeNet, self).__init__()
        self.conv1 = nn.Conv2d(3, 96, kernel_size=5)  # 3, 6, k=5
        self.bn0 = nn.BatchNorm2d(96)
        self.maxpool = nn.MaxPool2d(3, 2)
        self.conv2 = nn.Conv2d(96, 128, kernel_size=3)
        self.bn1 = nn.BatchNorm2d(128)
        self.conv3 = nn.Conv2d(128, 256, kernel_size=3)
        self.bn2 = nn.BatchNorm2d(256)
        self.fc1 = nn.Linear(256, 2000)
        self.fc2 = nn.Linear(2000, features_size)
        self.fc3 = nn.Linear(features_size, num_classes)

    def forward(self, x):
        x = self.maxpool(F.relu(self.bn0(self.conv1(x))))
        x = self.maxpool(F.relu(self.bn1(self.conv2(x))))
        x = self.maxpool(F.relu(self.bn2(self.conv3(x))))
        x = x.view(x.size(0), -1)
        x = F.relu(self.fc1(x))
        features = F.relu(self.fc2(x))
        output = (self.fc3(features))
        return features, output


if __name__ == '__main__':
    model = LeNet(num_classes=10, features_size=2048)
    x = torch.zeros(1, 3, 32, 32, dtype=torch.float, requires_grad=False)
    features, output = model(x)
    print(features.shape)
    assert list(features.shape) == [1, 2048]
    print(output.shape)
    assert list(output.shape) == [1, 10]
