import pydot
import torch
from torchviz import make_dot


def summary(model, filename):
    model.eval()
    x = torch.zeros(1, 3, 32, 32, dtype=torch.float, requires_grad=False)
    out = model(x)
    dot = make_dot(out, params=dict(model.named_parameters()))
    graphs = pydot.graph_from_dot_data(str(dot))
    graphs[0].write_svg(filename)
