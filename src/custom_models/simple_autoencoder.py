from __future__ import print_function

import torch
import torch.nn as nn


class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)


class SimpleAutoencoder(nn.Module):
    def __init__(self, input_size=2048, intermediate_size=84):
        super(SimpleAutoencoder, self).__init__()
        self.dense1 = nn.Linear(input_size, intermediate_size)
        self.dense2 = nn.Linear(intermediate_size, input_size)

    def forward(self, x):
        features = self.dense1(x)
        relu_dense_1 = torch.relu(features)
        output = self.dense2(relu_dense_1)
        return features, output


class SimpleAutoencoderClassifier(nn.Module):
    def __init__(self, input_size=2048, intermediate_size=84, num_classes=10):
        super(SimpleAutoencoderClassifier, self).__init__()
        self.dense1 = nn.Linear(input_size, intermediate_size)
        self.dense2 = nn.Linear(intermediate_size, input_size)
        self.dense3 = nn.Linear(intermediate_size, num_classes)

    def forward(self, x):
        features = self.dense1(x)
        relu_dense_1 = torch.relu(features)
        reconstructed = self.dense2(relu_dense_1)
        output = self.dense3(relu_dense_1)
        return features, reconstructed, output


if __name__ == '__main__':
    model = SimpleAutoencoder(input_size=2048, intermediate_size=84)
    x = torch.zeros(1, 2048, dtype=torch.float, requires_grad=False)
    features, output = model(x)
    print(features.shape)
    assert list(features.shape) == [1, 84]
    print(output.shape)
    assert list(output.shape) == [1, 2048]
