import torch
import torch.nn as nn


class AlexNet(nn.Module):
    def __init__(self, num_classes=10, features_size=2048):
        super(AlexNet, self).__init__()
        self.features = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=3, stride=2, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2),
            nn.Conv2d(64, 192, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2),
            nn.Conv2d(192, 384, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(384, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2),
        )
        self.feature_2 = nn.Sequential(
            nn.Dropout(),
            nn.Linear(256 * 2 * 2, 4096),
            nn.ReLU(inplace=True),
            nn.Dropout(),
            nn.Linear(4096, 4096),
            nn.ReLU(inplace=True),
            nn.Linear(4096, features_size),
        )
        self.classifier = nn.Linear(features_size, num_classes)

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), 256 * 2 * 2)
        features = self.feature_2(x)
        logits = self.classifier(features)
        return features, logits


if __name__ == '__main__':
    num_classes = 10
    features_size = 2048
    model = AlexNet(num_classes=num_classes, features_size=features_size)
    # summary(model, 'models/MobileNetV2.svg')
    x = torch.zeros(1, 3, 32, 32, dtype=torch.float, requires_grad=False)
    features, output = model(x)
    print(features.shape)
    assert list(features.shape) == [1, features_size]
    print(output.shape)
    assert list(output.shape) == [1, num_classes]
