import matplotlib.pyplot as plt


def get_value_after(log_path, v):
    with open(log_path, "r") as f:
        logs = str(f.readlines())
    splits_1 = logs.split(v)[1:]
    x = []
    y = []
    for epoch, split in enumerate(splits_1):
        accuracy = float(split.split('\\n')[0].strip())
        x.append(epoch + 1)
        y.append(accuracy)
    return x, y


if __name__ == '__main__':
    model_name = 'alexnet'
    dataset = 'cifar10'
    log_path_1 = f'logs/{dataset}/{model_name}_no_transfer.log'
    log_path_2 = f'logs/{dataset}/{model_name}_aug_0.log'
    log_path_3 = f'logs/{dataset}/{model_name}_aug_178.log'
    epochs = 200

    selection_string = 'Accuracy(multiclass):'

    for log_path, label in zip([log_path_1, log_path_2, log_path_3],
                               ['without teacher', 'with teacher', 'with data augmentation']):
        try:
            x, y = get_value_after(log_path, selection_string)
            plt.plot(x[:epochs], y[:epochs], label=label)
        except:
            print('ERROR:', log_path)

    plt.ylabel('Test Accuracy')
    plt.xlabel('Epoch')
    if model_name == 'alexnet':
        plt.ylim(0.65, 0.75)
    plt.legend()
    plt.savefig(f'documentation_images/{dataset}_{model_name}_accuracy.pdf')
