import cifar10_models
from src.custom_models.alexnet import AlexNet
from src.custom_models.lenet5 import LeNet
from src.custom_models.mobile_ne_v2 import mobilenet_v2
from src.custom_models.resnet import resnet18
from matplotlib import pyplot as plt

from src.custom_models.resnet_cifar100 import cifar_resnet56

dataset = 'cifar_100'
if dataset == 'cifar10':
    teacher = cifar10_models.resnet50(pretrained=False)
    teacher_name = ' Resnet50'
    num_classes = 10
    features_size = 2048
else:
    teacher = cifar_resnet56()
    teacher_name = ' Resnet56'
    num_classes = 100
    features_size = 64

lenet = LeNet(num_classes=num_classes, features_size=features_size)
resnet18_f = resnet18(pretrained=False, **{'num_classes': num_classes, 'features_size': features_size})
mobilenetv2 = mobilenet_v2(pretrained=False, **{'num_classes': num_classes, 'features_size': features_size})
alexnet = AlexNet(num_classes=num_classes, features_size=features_size)

if dataset == 'cifar10':
    improveing = [
        0.6549 - 0.6543,
        0.7399 - 0.7275,
        0.816 - 0.7482,
        0.8352 - 0.7876
    ]
else:
    improveing = [
        0.3386 - 0.2834,
        0.4049 - 0.3586,
        0.4325 - 0.3131,
        0.6077 - 0.5182
    ]

models = [
    ['Lenet5', lenet, 'cyan', improveing[0]],
    ['Alexnet', alexnet, 'cyan', improveing[1]],
    ['MobileNetV2', mobilenetv2, 'cyan', improveing[2]],
    ['Resnet18', resnet18_f, 'cyan', improveing[3]],
]
x = []
y = []
s = []
percentual_better = []
colors = []
my_xticks = []

for i, (name, model, color, val) in enumerate(models):
    total_params = sum(p.numel() for p in model.parameters())
    # trainable_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    x.append(i)
    y.append(total_params)
    s.append(100000 * val)
    percentual_better.append(val * 100)
    my_xticks.append(name)
    colors.append(color)

full_x = x.copy()
full_x.append(4)
my_xticks.append(teacher_name)
plt.xticks(full_x, my_xticks)

plt.scatter(x, y, s=s, color=colors)
plt.scatter(4, sum(p.numel() for p in teacher.parameters()), marker='x', color=['black'])
for a, b, p in zip(x, y, percentual_better):
    plt.text(a - 0.23, b - 700000, f'{p:.2f}%', fontsize=12)

if dataset == 'cifar10':
    plt.ylim(-1e6, 3.5e7)
    plt.xlim(-0.3, 4.3)
else:
    plt.ylim(-6e6, 3.0e7)
    plt.xlim(-0.8, 4.3)

plt.ylabel('Num Parameters')
plt.xlabel('Models')

plt.savefig(f'documentation_images/{dataset}_paramiters.pdf')
