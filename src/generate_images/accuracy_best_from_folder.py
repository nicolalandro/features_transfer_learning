import os

from src.generate_images.accuracy_from_logs import get_value_after


def get_max_test_accuracy(log_path_1, selection_string):
    x, y = get_value_after(log_path_1, selection_string)
    return max(y)


if __name__ == '__main__':
    folder_name = 'cifar10_inceptionv3'
    folder = f'../../logs/{folder_name}'
    files = os.listdir(folder)

    selection_string = 'Accuracy(multiclass):'

    for file_name in files:
        log_path = os.path.join(folder, file_name)
        print(log_path)
        try:
            value = get_max_test_accuracy(log_path, selection_string)
            print('\t', value)
        except Exception as e:
            print('\tError:', e)
