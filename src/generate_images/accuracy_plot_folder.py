import os

import matplotlib.pyplot as plt
import numpy as np


def get_value_after(log_path, v):
    with open(log_path, "r") as f:
        logs = str(f.readlines())
    splits_1 = logs.split(v)[1:]
    x = []
    y = []
    for epoch, split in enumerate(splits_1):
        try:
            accuracy = float(split.split('\\n')[0].strip())
        except:
            accuracy = float(split.split(' ')[1].strip())
        x.append(epoch + 1)
        y.append(accuracy)
    return x, y


if __name__ == '__main__':
    dataset = 'cifar10_inceptionv3'
    logs = os.listdir(f'logs/{dataset}/')
    logs.sort()

    selection_string = 'Accuracy(multiclass):'

    plt.figure(figsize=(3, 1))
    f, (ax, axleg) = plt.subplots(2, figsize=(10, 10))

    for filename in logs:
        log_path = f'logs/{dataset}/{filename}'
        try:
            x, y = get_value_after(log_path, selection_string)
            print(filename, 'accuracy:', max(y), f'(at epoch {y.index(max(y))})', 'epochs:', len(x))
            ax.plot(x, y, label=log_path.split('/')[-1])
        except:
            try:
                x, y = get_value_after(log_path, 'acc:')
                print(filename, 'accuracy:', max(y), f'(at epoch {y.index(max(y))})', 'epochs:', len(x))
                ax.plot(x, y, label=log_path.split('/')[-1])
            except:
                print('ERROR:', log_path)

    ax.set_ylabel('Test Accuracy')
    ax.set_xlabel('Epoch')
    ax.set_title(dataset)
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.3), shadow=True, ncol=1)
    ax.set_yticks(np.arange(0.20, 0.95, .05))

    axleg.axis('off')
    # plt.show()
    plt.savefig(f'documentation_images/{dataset}_folder_accuracy.pdf')
