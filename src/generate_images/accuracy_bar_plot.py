import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from src.generate_images.accuracy_best import get_max_test_accuracy

dataset = 'cifar10'

labels = ['Lent5', 'Alexnet', 'MobileNetV2', 'Resnet18']
without_teacher_accuracy = []
with_teacher_accuracy = []
with_aug_accuracy = []

for model_name in ['lenet', 'alexnet', 'mobile_net' if dataset == 'cifar10' else 'mobilenetv2', 'resnet18']:
    log_path_1 = f'logs/{dataset}/{model_name}_no_transfer.log'
    value = get_max_test_accuracy(log_path_1, 'Accuracy(multiclass):')
    without_teacher_accuracy.append(value)
    log_path_2 = f'logs/{dataset}/{model_name}_aug_0.log'
    value = get_max_test_accuracy(log_path_2, 'Accuracy(multiclass):')
    with_teacher_accuracy.append(value)
    log_path_3 = f'logs/{dataset}/{model_name}_aug_178.log'
    value = get_max_test_accuracy(log_path_3, 'Accuracy(multiclass):')
    with_aug_accuracy.append(value)

x = np.arange(len(labels))  # the label locations
width = 0.20  # the width of the bars

numbers_of_bars = 3
rects1 = plt.bar(x - width, without_teacher_accuracy, width, label='Without teacher')
rects2 = plt.bar(x, with_teacher_accuracy, width, label='With teacher')
rects3 = plt.bar(x + width, with_aug_accuracy, width, label='With data augmentation')

# Add some text for labels, title and custom x-pltis tick labels, etc.

if dataset == 'cifar10':
    plt.ylim(0, 1.2)
    accuracy = 0.9438
else:
    plt.ylim(0, 0.9)
    accuracy = 0.7064
plt.axhline(y=accuracy, color='red')
plt.annotate('teacher accuracy', xy=(0.1, accuracy), ha='center', va='bottom', color='red')

plt.xticks(x, labels)
plt.legend()
plt.ylabel('Test Accuracy')
# plt.xlabel('Models')

#
# def autolabel(rects):
#     """Attach a text label above each bar in *rects*, displaying its height."""
#     for rect in rects:
#         height = rect.get_height()
#         plt.annotate(f'{height*100:.2f}%',
#                      xy=(rect.get_x() + rect.get_width() / numbers_of_bars, height),
#                      xytext=(0, 3),  # 3 points vertical offset
#                      textcoords="offset points",
#                      ha='center',
#                      va='bottom',
#                      fontsize=3)
#
#
# autolabel(rects1)
# autolabel(rects2)
# autolabel(rects3)

plt.savefig(f'documentation_images/{dataset}_accuracy_barplot.pdf')
