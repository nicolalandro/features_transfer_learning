from src.generate_images.accuracy_from_logs import get_value_after


def get_max_test_accuracy(log_path_1, selection_string):
    x, y = get_value_after(log_path_1, selection_string)
    return max(y)


if __name__ == '__main__':
    model_name = 'resnet18'
    dataset = 'cifar_100'
    log_path_1 = f'logs/{dataset}/{model_name}_no_transfer.log'
    log_path_2 = f'logs/{dataset}/{model_name}_aug_0.log'
    log_path_3 = f'logs/{dataset}/{model_name}_aug_178.log'

    selection_string = 'Accuracy(multiclass):'

    for log_path in [log_path_1, log_path_2, log_path_3]:
        print(log_path)
        try:
            value = get_max_test_accuracy(log_path, selection_string)
            print('\t', value)
        except Exception as e:
            print('\tError:', e)
