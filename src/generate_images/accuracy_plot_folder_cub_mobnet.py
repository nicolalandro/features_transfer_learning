import os

import matplotlib.pyplot as plt
import numpy as np


def get_value_after(log_path, v):
    with open(log_path, "r") as f:
        logs = str(f.readlines())
    splits_1 = logs.split(v)[1:]
    x = []
    y = []
    for epoch, split in enumerate(splits_1):
        try:
            accuracy = float(split.split('\\n')[0].strip())
        except:
            accuracy = float(split.split(' ')[1].strip())
        x.append(epoch + 1)
        y.append(accuracy)
    return x, y


if __name__ == '__main__':
    dataset = 'loretta_dogs_cosine_012teacher'
    logs = os.listdir(f'logs/{dataset}/')
    logs = list(filter(lambda x: 'mobile' in x, logs))
    logs.sort()

    label_map = {
        "mobilenet_original.log": "MobileNetV3",
        "mobilenetv3_2teacher_2layer_classifier.log": "MobileNetV3 (Resnet50 + Nts-Net)",
        "mobilenetv3_tramslator_ntsnet.log": "MobileNetV3 (Nts-Net)",
        "mobilenetv3_translator_resnet50.log": "MobileNetV3 (Resnet50)",
        "mobilenetv3-ntsSOD-resnet50imagenet_2d9add1377431ae720ff1d53241405aff23dbf01.log":  "MobileNetV3 (Resnet50 ImageNet + Nts-Net)"
    }

    selection_string = 'Accuracy(multiclass):'

#    plt.figure(figsize=(1))
    ax = plt.gca()

    for filename in logs:
        log_path = f'logs/{dataset}/{filename}'
        try:
            x, y = get_value_after(log_path, selection_string)
            print(filename, 'accuracy:', max(y), f'(at epoch {y.index(max(y))})', 'epochs:', len(x))
            ax.plot(x, y, label=label_map[log_path.split('/')[-1]])
        except:
            try:
                x, y = get_value_after(log_path, 'acc:')
                print(filename, 'accuracy:', max(y), f'(at epoch {y.index(max(y))})', 'epochs:', len(x))
                ax.plot(x, y, label=label_map[log_path.split('/')[-1]])
            except Exception as e:
                print(e)
                print('ERROR:', log_path)

    ax.set_ylabel('Test Accuracy')
    ax.set_xlabel('Epoch')
    #ax.set_title(dataset)
    # ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.3), shadow=True, ncol=1)
    ax.set_yticks(np.arange(0.20, 0.60, .05))
    ax.set_ylim(0.20, 0.60)
    ax.legend()

    #axleg.axis('off')
    # plt.show()
    plt.savefig(f'documentation_images/{dataset}_folder_accuracy.pdf')
