import os
from PIL import Image
import torchvision
from tqdm import tqdm
from torchvision import transforms

from src.custom_models.nts_net.model import ntsnet

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

import torch

use_gpu = torch.cuda.is_available()

import torch.nn.functional as F
from ignite.metrics import Accuracy
from torch import nn

from src.custom_models.resnet_cifar100 import cifar_resnet56

batch_size = 80
test_path = '/media/mint/Caddy/Datasets/CUB_200_2011/test'

transform_test = transforms.Compose([
    transforms.Resize((600, 600), Image.BILINEAR),
    transforms.CenterCrop((448, 448)),
    # transforms.RandomHorizontalFlip(), # solo se train
    transforms.ToTensor(),
    transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
])
testset = torchvision.datasets.ImageFolder(root=test_path, transform=transform_test)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=2)

net = ntsnet('models/nts_net_moreno.ckpt')
net = nn.DataParallel(net).cuda()

net.eval()
total = 0

test_crossentropy_loss = 0
accuracy_metric = Accuracy()
with torch.no_grad():
    for data, labels_categorical in tqdm(testloader):
        labels_categorical = labels_categorical.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        total += labels_categorical.size(0)

        concat_out, raw_logits, concat_logits, part_logits, top_n_index, top_n_prob = net(data)
        # print(concat_out.shape)
        # print(raw_logits.shape)
        # exit()

        test_crossentropy_loss += F.cross_entropy(concat_logits, labels_categorical, reduction='sum').item()

        accuracy_metric.update((concat_logits, labels_categorical))

test_crossentropy_loss /= total
accuracy = accuracy_metric.compute()
print(f'Crossentropy(reduction sum): {test_crossentropy_loss}, Accuracy({accuracy_metric._type}): {accuracy}')
