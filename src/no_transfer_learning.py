import argparse
import datetime
import os
import time

import torch
import torch.nn as nn
import torch.nn.functional as F
from ignite.metrics import Accuracy
from torch.optim import lr_scheduler

from cifar10_models import resnet50
from src.custom_datasets.read_dataset_torch import get_dataset_loaders
from src.learn_transfer import create_model
from src.utils import AverageMeter

parser = argparse.ArgumentParser("Transfer Knowledge from resnet 50")

# dataset
parser.add_argument('-d', '--dataset', type=str, default='cifar10',
                    choices=['mnist', 'fashion_MNIST', 'cifar10', 'cifar100', 'Caltech101', 'cifar2', 'BioDatasetsLoad',
                             'MyDataset',
                             'WrongDataset'])
parser.add_argument('--path', type=str, default='/home/supreme/datasets/cifar10')

# optimization
parser.add_argument('--batch-size', type=int, default=128)
parser.add_argument('--lr', type=float, default=0.001, help="learning rate for model")
parser.add_argument('--stepsize', type=int, default=50)
parser.add_argument('--gamma', type=float, default=0.1, help="learning rate decay")

# model
parser.add_argument('--simple-model', type=str, default='lenet',
                    choices=['lenet', 'resnet18', 'mobilenetV2', 'alexnet', 'lenet_paper'])
parser.add_argument('--max-epoch', type=int, default=200)
parser.add_argument('--when_save_model', type=int, default='10', help="save model at epoch x")
parser.add_argument('--save-dir', type=str, default='models')

# pythorch seed
parser.add_argument('--seed', type=int, default=1, help='using a fixed seed to be reproducible')

# selected gpus
parser.add_argument('--gpu', type=str, default='1', help='select the gpu or gpus to use')

args = parser.parse_args()


def main():
    torch.manual_seed(args.seed)

    # args.simple_model = 'resnet50_cifar100'
    print('--- START Arg parse ---')
    options = vars(args)
    for k, v in options.items():
        print(k, ':', v)
    print('--- END Arg parse ---')

    test_gpu_settings()

    trainloader, testloader, num_classes = get_dataset_loaders(args.path, args.batch_size)
    features_size = 2048
    resnet_classifier = resnet50(pretrained=True)

    resnet_modules = list(resnet_classifier.children())[:-1]
    resnet = nn.Sequential(*resnet_modules)
    resnet = nn.DataParallel(resnet).cuda()

    resnet_classifier = nn.DataParallel(resnet_classifier).cuda()

    model = create_model(args.simple_model, features_size, num_classes)
    model = nn.DataParallel(model).cuda()

    criterion_mse = nn.MSELoss().cuda()
    criterion_xent = nn.CrossEntropyLoss().cuda()

    params = list(model.parameters())
    optimizer = torch.optim.Adam(params, args.lr)
    # optimizer = torch.optim.RMSprop(params, lr=args.lr, alpha=0.99, eps=1e-8, momentum=0,
    #                                 centered=False)

    scheduler = lr_scheduler.StepLR(optimizer, step_size=args.stepsize, gamma=args.gamma)

    start_time = time.time()
    for epoch in range(args.max_epoch):
        print("==> Start Epoch {}/{}".format(epoch + 1, args.max_epoch))
        train_model(resnet, resnet_classifier, model, trainloader, criterion_mse, criterion_xent,
                    optimizer)

        scheduler.step()

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

        test(resnet, model, testloader, num_classes)

        if (epoch + 1) % args.when_save_model == 0:  # Save trained model
            print("Model trained saveing...")
            torch.save(model,
                       f'{args.save_dir}/no_transfer_{args.simple_model}_{epoch + 1}.pt')
        print("==> End Epoch {}/{}".format(epoch + 1, args.max_epoch))


def train_model(resnet, resnet_classifier, model, trainloader, criterion_mse, criterion_xent, optimizer):
    model.train()
    mse_losses = AverageMeter()
    xent_losses = AverageMeter()

    for batch_idx, (data, labels) in enumerate(trainloader):
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        resnet_features = resnet(data)
        resnet_features = resnet_features.view(-1, 2048)

        if args.simple_model == 'resnet50_cifar100':
            lenet_predictions = model(data)
            lenet_features = resnet_features
        else:
            lenet_features, lenet_predictions = model(data)
        loss_mse = criterion_mse(lenet_features, resnet_features)
        loss_xent = criterion_xent(lenet_predictions, labels)

        # loss = loss_xent
        loss = loss_xent

        optimizer.zero_grad()
        loss.backward()

        optimizer.step()

        mse_losses.update(loss_mse.item(), labels.size(0))
        xent_losses.update(loss_xent.item(), labels.size(0))

        if (batch_idx + 1) % 80 == 0:
            print(
                f"Batch {batch_idx + 1}\t  MSE {mse_losses.val} ({mse_losses.avg}), CrossEntropy {xent_losses.val} ({xent_losses.avg})")


def test(resnet, model, testloader, num_classes):
    model.eval()
    total = 0

    test_mse_loss = 0
    test_crossentropy_loss = 0
    accuracy_metric = Accuracy()
    with torch.no_grad():
        for data, labels_categorical in testloader:
            labels_categorical = labels_categorical.cuda(non_blocking=True)
            data = data.cuda(non_blocking=True)

            total += labels_categorical.size(0)

            resnet_features = resnet(data)
            resnet_features = resnet_features.view(-1, 2048)
            if args.simple_model == 'resnet50_cifar100':
                lenet_predictions = model(data)
                lenet_features = resnet_features
            else:
                lenet_features, lenet_predictions = model(data)

            test_mse_loss += F.mse_loss(lenet_features, resnet_features, reduction='sum').item()
            test_crossentropy_loss += F.cross_entropy(lenet_predictions, labels_categorical, reduction='sum').item()

            accuracy_metric.update((lenet_predictions, labels_categorical))

    test_mse_loss /= total
    test_crossentropy_loss /= total
    accuracy = accuracy_metric.compute()
    print(
        f'Test MSE(reduction sum): {test_mse_loss}, Crossentropy(reduction sum): {test_crossentropy_loss}, Accuracy({accuracy_metric._type}): {accuracy}')


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    # sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
    #     args.percentage_used) + '.txt'))
    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        print(f"Learning rate optimizer {args.lr}  Batch-size {args.batch_size}")
    else:
        print("Currently using CPU")
        exit()


if __name__ == '__main__':
    main()
