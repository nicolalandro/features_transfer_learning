import os
from random import randrange

import PIL.Image
import pydot
import torch
import torch.nn as nn
import torchvision
from torchviz import make_dot

from src.custom_models.lenet5 import Flatten

weight_path = 'models/lenet_20(random size: 0).pt'
num_classes = 10
class_list = {0: 'airplane', 1: 'automobile', 2: 'bird', 3: 'cat', 4: 'deer', 5: 'dog', 6: 'frog', 7: 'horse',
              8: 'ship', 9: 'truck'}
base_path = '/home/super/datasets-nas/cifar10/test/'
random_class = class_list[randrange(10)]
rand_img_name = f'{randrange(1000):04}.png'
sample_img = os.path.join(base_path, random_class, rand_img_name)

train_transforms = torchvision.transforms.Compose([
    # torchvision.transforms.Resize(size=(32, 32)),
    torchvision.transforms.ToTensor()
])


def manual_prediction():
    lenet_features, lenet_predictions = model(tensor_images)
    print('prediction: ', lenet_predictions)
    pred_categorical = lenet_predictions.argmax(dim=1, keepdim=True).reshape(-1)
    class_id = pred_categorical.data.cpu().numpy()[0]
    print('class id:', class_id)
    extracted_class = class_list[class_id]
    print('class_name', extracted_class)
    return extracted_class


def summary(model):
    model.eval()
    x = torch.zeros(1, 3, 32, 32, dtype=torch.float, requires_grad=False)
    out = model(x)
    dot = make_dot(out, params=dict(model.named_parameters()))
    graphs = pydot.graph_from_dot_data(str(dot))
    graphs[0].write_svg('logs/torch_lenet5_custom.svg')


if __name__ == '__main__':
    print('model path:', weight_path)
    model = torch.load(weight_path)
    # summary(model)
    model.eval()

    print('image_path:', sample_img)
    image = PIL.Image.open(sample_img)
    tensor_image = train_transforms(image)
    tensor_images = tensor_image.reshape(1, 3, 32, 32)
    lenet_features, lenet_predictions = model(tensor_images)
    print('prediction: ', lenet_predictions)
    pred_categorical = lenet_predictions.argmax(dim=1, keepdim=True).reshape(-1)
    class_id = pred_categorical.data.cpu().numpy()[0]
    print('class id:', class_id)
    extracted_class = class_list[class_id]
    print('class_name', extracted_class)

    lenet = list(model.children())[0].cpu()

    print(lenet.conv1)
    layers = list(lenet.children())

    layer_results = [tensor_images]
    for layer in layers:
        print('forward', layer)
        forword_result = layer.forward(layer_results[-1])
        layer_results.append(forword_result)

    winner = layer_results[-1].argmax(dim=1, keepdim=True).reshape(-1)[0].numpy()
    select_top = 5
    print('winner class:', winner)
    for_size = len(layers) - 1
    for i in range(0, for_size + 1):
        layer_number = for_size - i
        layer = layers[layer_number]
        print(layer_number, layer, type(layer))
        if type(layer) == nn.ReLU:
            pass
        elif type(layer) == Flatten:
            pass
        elif type(layer) == nn.modules.pooling.MaxPool2d:
            pass
        else:
            w = layer.weight[winner]
            b = layer.bias[winner]
            x = layer_results[layer_number]

            values = w * x + b
            topk = torch.topk(values, k=5, dim=1)
            print(topk)
            print('\t', values.shape)
            break