import os
from random import randrange

import PIL.Image
import matplotlib.pyplot as plt
import numpy as np
import torch
import torchvision
from lime import lime_image
from skimage.segmentation import mark_boundaries
from torchviz import make_dot
import pydot

weight_path = 'models/lenet_80(random size: 178).pt'
num_classes = 10
class_list = {0: 'airplane', 1: 'automobile', 2: 'bird', 3: 'cat', 4: 'deer', 5: 'dog', 6: 'frog', 7: 'horse',
              8: 'ship', 9: 'truck'}
base_path = '/home/super/datasets-nas/cifar10/test/'
random_class = class_list[randrange(10)]
rand_img_name = f'{randrange(1000):04}.png'
sample_img = os.path.join(base_path, random_class, rand_img_name)

train_transforms = torchvision.transforms.Compose([
    # torchvision.transforms.Resize(size=(32, 32)),
    torchvision.transforms.ToTensor()
])


def manual_prediction():
    lenet_features, lenet_predictions = model(tensor_images)
    print('prediction: ', lenet_predictions)
    pred_categorical = lenet_predictions.argmax(dim=1, keepdim=True).reshape(-1)
    class_id = pred_categorical.data.cpu().numpy()[0]
    print('class id:', class_id)
    extracted_class = class_list[class_id]
    print('class_name', extracted_class)
    return extracted_class


def summary(model):
    model.eval()
    x = torch.zeros(1, 3, 32, 32, dtype=torch.float, requires_grad=False)
    out = model(x)
    dot = make_dot(out, params=dict(model.named_parameters()))
    graphs = pydot.graph_from_dot_data(str(dot))
    graphs[0].write_svg('logs/torch_lenet5_custom.svg')


if __name__ == '__main__':
    print('model path:', weight_path)
    model = torch.load(weight_path)
    summary(model)
    model.eval()

    print('image_path:', sample_img)
    image = PIL.Image.open(sample_img)
    tensor_image = train_transforms(image)
    tensor_images = tensor_image.reshape(1, 3, 32, 32)

    extracted_class_name = manual_prediction()


    def prediction_for_lime(images):
        model.eval()
        image_tensor = torch.tensor(np.rollaxis(images, 3, 1))
        lenet_features, lenet_predictions = model(image_tensor)
        return lenet_predictions.detach().cpu().numpy()


    lime_input = np.array(image)
    explainer = lime_image.LimeImageExplainer()
    explanation = explainer.explain_instance(np.rollaxis(tensor_image.detach().cpu().numpy(), 0, 3),
                                             prediction_for_lime,  # classification function
                                             top_labels=5)

    f, axarr = plt.subplots(2, 2)
    f.suptitle(f'Expected: {random_class}, Predicted: {extracted_class_name}', fontsize=16)

    axarr[0, 0].imshow(image)
    axarr[0, 0].set_title('original image')

    temp, mask = explanation.get_image_and_mask(explanation.top_labels[0],
                                                positive_only=False,
                                                num_features=5,
                                                hide_rest=True)
    axarr[0, 1].imshow(mark_boundaries(temp / 2 + 0.5, mask))
    axarr[0, 1].set_title('masked image 1')

    temp, mask = explanation.get_image_and_mask(explanation.top_labels[1],
                                                positive_only=False,
                                                num_features=5,
                                                hide_rest=True)
    axarr[1, 0].imshow(mark_boundaries(temp / 2 + 0.5, mask))
    axarr[1, 0].set_title('masked image 2')

    temp, mask = explanation.get_image_and_mask(explanation.top_labels[2],
                                                positive_only=False,
                                                num_features=5,
                                                hide_rest=True)
    axarr[1, 1].imshow(mark_boundaries(temp / 2 + 0.5, mask))
    axarr[1, 1].set_title('masked image 3')

    plt.savefig('logs/lenet5_cifar10.png')
