from __future__ import print_function

import itertools

import torch
import torch.nn as nn


class TranslatorAutoencoder(nn.Module):
    def __init__(self, input_size=10240, intermediate_size=960):
        super(TranslatorAutoencoder, self).__init__()
        self.dense1 = nn.Linear(input_size, intermediate_size)
        self.dense2 = nn.Linear(intermediate_size, input_size)

    def forward(self, x):
        encoded = self.dense1(x)
        decoded = self.dense2(encoded)
        return encoded, decoded


class TranslatorAutoencoderClassifier(nn.Module):
    def __init__(self, input_size=10240, intermediate_size=960, num_classes=131):
        super(TranslatorAutoencoderClassifier, self).__init__()
        self.dense1 = nn.Linear(input_size, intermediate_size)
        self.dense2 = nn.Linear(intermediate_size, input_size)
        self.dense3 = nn.Linear(intermediate_size, num_classes)

    def forward(self, x):
        encoded = self.dense1(x)
        decoded = self.dense2(encoded)
        output = self.dense3(encoded)
        return encoded, decoded, output


class TranslatorClassifier(nn.Module):
    def __init__(self, input_size=10240, intermediate_size=960, num_classes=131):
        super(TranslatorClassifier, self).__init__()
        self.dense1 = nn.Linear(input_size, intermediate_size)
        self.dense3 = nn.Linear(intermediate_size, num_classes)

    def forward(self, x):
        encoded = self.dense1(x)
        output = self.dense3(encoded)
        return encoded, output

    def translator_only_paramiters(self):
        return itertools.chain(self.dense1.parameters(), )


class MultiTranslatorClassifier(nn.Module):
    def __init__(self, input_size1=10240, input_size2=2048, intermediate_size=960, num_classes=131):
        super(MultiTranslatorClassifier, self).__init__()
        self.dense1 = nn.Linear(input_size1, 1024)
        self.dense2 = nn.Linear(input_size2, 1024)

        self.intermediate = nn.Linear(2048, intermediate_size)

        self.dense3 = nn.Linear(intermediate_size, num_classes)

    def forward(self, f1, f2):
        a = self.dense1(f1)
        b = self.dense2(f2)
        concat = torch.cat((a, b), dim=1)

        features = self.intermediate(concat)
        output = self.dense3(features)
        return features, output

    def translator_only_paramiters(self):
        return itertools.chain(self.dense1.parameters(), )


class MultiTranslatorClassifierConcat(nn.Module):
    def __init__(self, input_size1=10240, input_size2=2048, intermediate_size=960, num_classes=131):
        super(MultiTranslatorClassifierConcat, self).__init__()
        self.dense1 = nn.Linear(input_size1, intermediate_size // 2)
        self.dense2 = nn.Linear(input_size2, intermediate_size // 2)

        self.dense3 = nn.Linear(intermediate_size, num_classes)

    def forward(self, f1, f2):
        a = self.dense1(f1)
        b = self.dense2(f2)
        features = torch.cat((a, b), dim=1)
        output = self.dense3(features)
        return features, output

    def translator_only_paramiters(self):
        return itertools.chain(self.dense1.parameters(), )


class TwoTranslator(nn.Module):
    def __init__(self, input_size1=10240, input_size2=2048, intermediate_size1=960, intermediate_size2=1280):
        super(TwoTranslator, self).__init__()
        self.dense1 = nn.Linear(input_size1, intermediate_size1)
        self.dense2 = nn.Linear(input_size2, intermediate_size2)

    def forward(self, f1, f2):
        features1 = self.dense1(f1)
        features2 = self.dense2(f2)
        return features1, features2

    def translator_only_paramiters(self):
        return itertools.chain(self.dense1.parameters(), )


class TwoTranslatorClassifier(nn.Module):
    def __init__(self, input_size1=10240, input_size2=2048, intermediate_size1=960, intermediate_size2=1280,
                 inceptionv3=False):
        super(TwoTranslatorClassifier, self).__init__()
        self.inceptionv3 = inceptionv3
        self.dense1 = nn.Linear(input_size1, intermediate_size1)
        self.dense2 = nn.Linear(input_size2, intermediate_size2)

        self.intermediate = None
        self.classifier = None

    def forward(self, f1, f2):
        features1 = self.dense1(f1)
        features2 = self.dense2(f2)

        if self.inceptionv3:
            features1_r = features1.reshape(-1, 2048, 7, 7)

            out1 = self.intermediate(features1_r)
            out1 = out1.view(out1.size(0), -1)
            out1 = self.classifier(out1)
        else:
            out1 = self.intermediate(features1)
            out1 = self.classifier(out1)

        out2 = self.classifier(features2)
        return features1, features2, out1, out2

    def translator_only_paramiters(self):
        return itertools.chain(self.dense1.parameters(), self.dense2.parameters())


if __name__ == '__main__':
    model = TranslatorAutoencoder(input_size=2048, intermediate_size=84)
    x = torch.zeros(1, 2048, dtype=torch.float, requires_grad=False)
    features, output = model(x)
    print(features.shape)
    assert list(features.shape) == [1, 84]
    print(output.shape)
    assert list(output.shape) == [1, 2048]
