import os

import torch
from PIL import Image
from torch import nn
from torchvision import transforms
from tqdm import tqdm

from src.custom_datasets.read_image_and_name import ImageAndNameFolder
from src.demo.resnet import features_resnet

use_gpu = torch.cuda.is_available()

output_path = '/media/mint/Barracuda/Datasets/LorettaDogs/resnet50-imagenet-features/'
dataset_path = '/media/mint/Barracuda/Datasets/LorettaDogs/'
environment = 'test'

transform_test = transforms.Compose([
    transforms.Resize((224, 224), Image.BILINEAR),
    transforms.ToTensor(),
])
testset = ImageAndNameFolder(root=dataset_path + environment, transform=transform_test)
testloader = torch.utils.data.DataLoader(testset, batch_size=60, shuffle=False, num_workers=2)

device = 'cuda'
# num_classes = 131
net = features_resnet.resnet50(pretrained=True)
# net.avgpool = nn.AdaptiveAvgPool2d(1)
# net.fc = nn.Linear(512 * 4, num_classes)
# state_dict = torch.load(
#     '/media/mint/Caddy/Datasets/LorettaDogs/ModelsFeatureTransfer/resnet50/lorettadogs_resnet50_pretrained_4.pt')
# net.load_state_dict(state_dict)
# net = torch.load(
#     '/media/mint/Caddy/Datasets/LorettaDogs/ModelsFeatureTransfer/resnet50/lorettadogs_resnet50_pretrained_4.pt')
net = nn.DataParallel(net).cuda()

net.eval()

with torch.no_grad():
    for data, img_names in tqdm(testloader):
        data = data.to(device)
        _, features, _ = net(data)

        for img, file_name in zip(features, img_names):
            splitted_filename = file_name.split('/')
            folder_path = os.path.join(output_path, environment, splitted_filename[-2])
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)
            file_output_path = os.path.join(folder_path, splitted_filename[-1] + '.pt')
            torch.save(img.to('cpu'), file_output_path)
