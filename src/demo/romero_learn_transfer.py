import argparse
import datetime
import os
import time

import torch
import torch.nn as nn
from PIL import Image
from ignite.metrics import Accuracy
from torch.optim import lr_scheduler
from torchvision import transforms
from tqdm import tqdm

from src.custom_datasets.image_feature_folder import ImageAndFeatureFolder
from src.custom_losses.distillation_loss import DistillationLoss
from src.custom_models.alexnet import AlexNet
from src.custom_models.lenet5 import LeNet
from src.custom_models.mobile_ne_v2 import mobilenet_v2
from src.custom_models.resnet import resnet18
from src.custom_models.simplified_inceptionv3 import inception_simplified
from src.demo.mobilenet import mobilenetv3
from src.utils import AverageMeter

parser = argparse.ArgumentParser("Transfer Knowledge from resnet 50")

# dataset
parser.add_argument('-d', '--dataset', type=str, default='cifar10',
                    choices=['cub200', 'cifar10'])
parser.add_argument('--path', type=str, default='/media/mint/Barracuda/Datasets/cifar100/')
parser.add_argument('--features-path', type=str,
                    default='/media/mint/Barracuda/Datasets/cifar100/resnet50-features-cpu/')
parser.add_argument('--teacher-outputs-path', type=str,
                    default='/media/mint/Barracuda/Datasets/cifar100/resnet50-outputs-cpu/')

# optimization
parser.add_argument('--batch-size', type=int, default=512)
parser.add_argument('--lr', type=float, default=0.001, help="learning rate for model")
parser.add_argument('--stepsize', type=int, default=200)
parser.add_argument('--gamma', type=float, default=0.1, help="learning rate decay")

# model
parser.add_argument('--simple-model', type=str, default='resnet18',
                    choices=['inceptionv3SIMPLY', 'lenet', 'mobilenetv3', 'mobilenetv2', 'alexnet', 'resnet18'])
parser.add_argument('--max-epoch', type=int, default=200)
parser.add_argument('--when_save_model', type=int, default='400', help="save model at epoch x")
parser.add_argument('--save-dir', type=str, default='models/mobilenet_transfer')

# pythorch seed
parser.add_argument('--seed', type=int, default=1, help='using a fixed seed to be reproducible')

# selected gpus
parser.add_argument('--gpu', type=str, default='0', help='select the gpu or gpus to use')

parser.add_argument('--romero-enabled', type=int, default=1, help='using romero pre train')
parser.add_argument('--distillation-enabled', type=int, default=1, help='using distillation loss')

args = parser.parse_args()


def main():
    torch.manual_seed(args.seed)

    print('--- START Arg parse ---')
    options = vars(args)
    for k, v in options.items():
        print(k, ':', v)
    print('--- END Arg parse ---')

    test_gpu_settings()

    image_shape = (32, 32)
    transform_train = transforms.Compose([
        transforms.Resize(image_shape, Image.BILINEAR),
        transforms.ToTensor(),
    ])
    trainset = ImageAndFeatureFolder(root=os.path.join(args.path, 'train'),
                                     features_root=args.features_path,
                                     transform=transform_train)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True, num_workers=2)

    trainset_dist = ImageAndFeatureFolder(root=os.path.join(args.path, 'train'),
                                          features_root=args.teacher_outputs_path,
                                          transform=transform_train)
    trainloader_dist = torch.utils.data.DataLoader(trainset_dist, batch_size=args.batch_size, shuffle=True,
                                                   num_workers=2)

    transform_test = transforms.Compose([
        transforms.Resize(image_shape, Image.BILINEAR),
        transforms.ToTensor(),
    ])
    testset = ImageAndFeatureFolder(root=os.path.join(args.path, 'test'),
                                    features_root=args.teacher_outputs_path,
                                    transform=transform_test)
    testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=2)

    num_classes = len(trainset.classes)
    print(trainset.classes)

    model = create_model(args.simple_model, num_classes)
    model = nn.DataParallel(model).cuda()

    features_size = 2048
    if args.simple_model == 'mobilenetv3':
        features_size = 960
    regressor = torch.nn.Linear(2048, features_size)
    regressor = nn.DataParallel(regressor).cuda()

    criterion_mse = nn.MSELoss().cuda()
    criterion_xent = nn.CrossEntropyLoss().cuda()
    criterion_distillation = DistillationLoss().cuda()

    params = list(model.parameters()) + list(regressor.parameters())
    optimizer_romero = torch.optim.Adam(params, args.lr)

    # if args.distillation_enabled == 1:
    #     dist_params = list(model.parameters()) + list(criterion_distillation.parameters())
    # else:
    #     dist_params = list(model.parameters())
    dist_params = list(model.parameters())
    optimizer_distillation = torch.optim.Adam(dist_params, args.lr)

    scheduler_romero = lr_scheduler.CosineAnnealingLR(optimizer_romero, args.max_epoch, 0.0001)
    scheduler_dist = lr_scheduler.CosineAnnealingLR(optimizer_distillation, args.max_epoch, 0.001)

    start_time = time.time()
    if args.romero_enabled == 1:
        for epoch in range(args.max_epoch):
            print("==> Start Romero Epoch {}/{}".format(epoch + 1, args.max_epoch))

            romero_train_model(model, regressor, trainloader, criterion_mse, optimizer_romero, epoch)
            scheduler_romero.step()

            elapsed = round(time.time() - start_time)
            elapsed = str(datetime.timedelta(seconds=elapsed))
            print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

    for epoch in range(args.max_epoch):
        print("==> Start Dist Epoch {}/{}".format(epoch + 1, args.max_epoch))

        distillation_train_model(model, trainloader_dist, criterion_xent, criterion_distillation,
                                 optimizer_distillation,
                                 epoch)
        scheduler_dist.step()

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

        test(model, testloader, criterion_xent, criterion_distillation)

        if (epoch + 1) % args.when_save_model == 0:  # Save trained model
            print("Model trained saveing...")
            torch.save(model,
                       f'{args.save_dir}/{args.simple_model}_{epoch + 1}.pt')
        print("==> End Epoch {}/{}".format(epoch + 1, args.max_epoch))


def create_model(model_name, num_classes):
    features_size = 2048
    if model_name == 'inceptionv3SIMPLY':
        model = inception_simplified.inception_v3()
    elif model_name == 'lenet':
        model = LeNet(num_classes=num_classes, features_size=features_size)
    elif model_name == 'mobilenetv3':
        model = mobilenetv3.mobilenetv3_large(**{'num_classes': num_classes, })
    elif model_name == 'resnet18':
        model = resnet18(pretrained=False, **{'num_classes': num_classes, 'features_size': features_size})
    elif model_name == 'mobilenetv2':
        model = mobilenet_v2(pretrained=False, **{'num_classes': num_classes, 'features_size': features_size})
    elif model_name == 'alexnet':
        model = AlexNet(num_classes=num_classes, features_size=features_size)
    return model


def distillation_train_model(model, trainloader, criterion_xent, criterion_distillation, optimizer_distillation, epoch):
    model.train()
    xent_losses = AverageMeter()
    dist_losses = AverageMeter()

    for batch_idx, (data, teacher_output, labels) in enumerate(trainloader):
        teacher_output = teacher_output.cuda(non_blocking=True)
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        _, predictions = model(data)

        loss_xent = criterion_xent(predictions, labels)

        if args.distillation_enabled == 1:
            loss_dist = criterion_distillation(predictions, teacher_output)
            loss = loss_xent + loss_dist
        else:
            loss = loss_xent

        optimizer_distillation.zero_grad()
        loss.backward()

        optimizer_distillation.step()

        xent_losses.update(loss_xent.item(), labels.size(0))
        if args.distillation_enabled == 1:
            dist_losses.update(loss_dist.item(), labels.size(0))

        if (batch_idx + 1) % 40 == 0:
            print(
                f"Dist Batch {batch_idx + 1}\t",
                f"CrossEntropy {xent_losses.val} ({xent_losses.avg})",
                f", Dist {xent_losses.val} ({xent_losses.avg})" if args.distillation_enabled == 1 else ''
            )


def romero_train_model(model, regressor, trainloader, criterion_mse, optimizer_romero, epoch):
    model.train()
    mse_losses = AverageMeter()

    for batch_idx, (data, features, labels) in enumerate(trainloader):
        teacher_features = features.cuda(non_blocking=True)
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        teacher_features_trans = regressor(teacher_features)
        features, predictions = model(data)

        loss_mse = criterion_mse(features, teacher_features_trans)

        optimizer_romero.zero_grad()
        loss_mse.backward()

        optimizer_romero.step()

        mse_losses.update(loss_mse.item(), labels.size(0))

        if (batch_idx + 1) % 40 == 0:
            print(
                f"Romero Batch {batch_idx + 1}\t "
                f" MSE {mse_losses.val} ({mse_losses.avg})"
            )


def test(model, testloader, criterion_xent, criterion_distillation):
    model.eval()
    total = 0

    test_dist = 0
    test_crossentropy_loss = 0
    accuracy_metric = Accuracy()
    with torch.no_grad():
        for data, teacher_features, labels_categorical in tqdm(testloader):
            teacher_features = teacher_features.cuda(non_blocking=True)
            labels_categorical = labels_categorical.cuda(non_blocking=True)
            data = data.cuda(non_blocking=True)

            total += labels_categorical.size(0)

            _, predictions = model(data)

            test_dist += criterion_distillation(predictions, teacher_features).item()
            test_crossentropy_loss += criterion_xent(predictions, labels_categorical).item()

            accuracy_metric.update((predictions, labels_categorical))

    test_dist /= total
    test_crossentropy_loss /= total
    accuracy = accuracy_metric.compute()
    print(
        f'Test Distillation: {test_dist}',
        f', alpha: {criterion_distillation.alpha.item()}, T: {criterion_distillation.T.item()}'
        f', Crossentropy(reduction sum): {test_crossentropy_loss}',
        f', Accuracy({accuracy_metric._type}): {accuracy}'
    )


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    # sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
    #     args.percentage_used) + '.txt'))
    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        print(f"Learning rate optimizer {args.lr}  Batch-size {args.batch_size}")
    else:
        print("Currently using CPU")
        exit()


if __name__ == '__main__':
    main()
