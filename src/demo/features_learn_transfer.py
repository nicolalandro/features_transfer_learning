import argparse
import datetime
import os
import time

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
from PIL import Image
from ignite.metrics import Accuracy
from torch.optim import lr_scheduler
from torchvision import transforms
from tqdm import tqdm

from src.custom_datasets.image_feature_folder import ImageAndFeatureFolder
from src.demo.mobilenet import mobilenetv3_mod, mobilenetv3
from src.utils import AverageMeter

parser = argparse.ArgumentParser("Transfer Knowledge from resnet 50")

# dataset
parser.add_argument('-d', '--dataset', type=str, default='cub200',
                    choices=['cub200'])
parser.add_argument('--path', type=str, default='/media/mint/Caddy/Datasets/LorettaDogs/')
parser.add_argument('--features-path', type=str,
                    default='/media/mint/Caddy/Datasets/LorettaDogs/nts-features-mobilenetv3-translation/')

# optimization
parser.add_argument('--batch-size', type=int, default=80)
parser.add_argument('--lr', type=float, default=0.001, help="learning rate for model")
parser.add_argument('--stepsize', type=int, default=200)
parser.add_argument('--gamma', type=float, default=0.1, help="learning rate decay")

# model
parser.add_argument('--teacher', type=str, default='ntsnet',
                    choices=['ntsnet', 'none', ])
parser.add_argument('--simple-model', type=str, default='mobilenetv3orig',
                    choices=['mobilenetv3orig', 'mobilenetv3'])
parser.add_argument('--max-epoch', type=int, default=200)
parser.add_argument('--when_save_model', type=int, default='1', help="save model at epoch x")
parser.add_argument('--save-dir', type=str, default='models/mobilenet_transfer')

# pythorch seed
parser.add_argument('--seed', type=int, default=1, help='using a fixed seed to be reproducible')

# selected gpus
parser.add_argument('--gpu', type=str, default='0', help='select the gpu or gpus to use')

args = parser.parse_args()


def main():
    torch.manual_seed(args.seed)

    print('--- START Arg parse ---')
    options = vars(args)
    for k, v in options.items():
        print(k, ':', v)
    print('--- END Arg parse ---')

    test_gpu_settings()

    image_shape = (244, 244)
    transform_train = transforms.Compose([
        transforms.Resize(image_shape, Image.BILINEAR),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
    ])
    trainset = ImageAndFeatureFolder(root=os.path.join(args.path, 'train'),
                                     features_root=args.features_path,
                                     transform=transform_train)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True, num_workers=2)

    transform_test = transforms.Compose([
        transforms.Resize(image_shape, Image.BILINEAR),
        transforms.ToTensor(),
    ])
    testset = ImageAndFeatureFolder(root=os.path.join(args.path, 'test'),
                                    features_root=args.features_path,
                                    transform=transform_test)
    testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=2)

    num_classes = len(trainset.classes)
    print(trainset.classes)
    features_size = 960

    model = create_model(args.simple_model, features_size, num_classes)
    model = nn.DataParallel(model).cuda()

    criterion_mse = nn.MSELoss().cuda()
    criterion_xent = nn.CrossEntropyLoss().cuda()

    params = list(model.parameters())
    optimizer = torch.optim.Adam(params, args.lr)

    scheduler = lr_scheduler.StepLR(optimizer, step_size=args.stepsize, gamma=args.gamma)

    start_time = time.time()
    for epoch in range(args.max_epoch):
        print("==> Start Epoch {}/{}".format(epoch + 1, args.max_epoch))
        train_model(model, trainloader, criterion_mse, criterion_xent, optimizer, epoch)

        scheduler.step()

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

        test(model, testloader)

        if (epoch + 1) % args.when_save_model == 0:  # Save trained model
            print("Model trained saveing...")
            torch.save(model,
                       f'{args.save_dir}/{args.simple_model}_{args.teacher}_{epoch + 1}.pt')
        print("==> End Epoch {}/{}".format(epoch + 1, args.max_epoch))


def create_model(model_name, features_size, num_classes):
    if model_name == 'mobilenetv3':
        model = mobilenetv3_mod.mobilenetv3_large(**{'num_classes': num_classes, 'features_size': features_size, })
    elif model_name == 'mobilenetv3orig':
        model = mobilenetv3.mobilenetv3_large(**{'num_classes': num_classes, })
    return model


def train_model(model, trainloader, criterion_mse, criterion_xent, optimizer, epoch):
    model.train()
    mse_losses = AverageMeter()
    xent_losses = AverageMeter()

    for batch_idx, (data, features, labels) in enumerate(trainloader):
        features = features.cuda(non_blocking=True)
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        lenet_features, lenet_predictions = model(data)

        loss_mse = criterion_mse(lenet_features, features)
        loss_xent = criterion_xent(lenet_predictions, labels)

        if args.teacher == 'none':
            loss = loss_xent
        else:
            loss = 0.01 * loss_mse + loss_xent

        optimizer.zero_grad()
        loss.backward()

        optimizer.step()

        mse_losses.update(loss_mse.item(), labels.size(0))
        xent_losses.update(loss_xent.item(), labels.size(0))

        if (batch_idx + 1) % 40 == 0:
            print(
                f"Batch {batch_idx + 1}\t  MSE {mse_losses.val} ({mse_losses.avg}), CrossEntropy {xent_losses.val} ({xent_losses.avg})")


def test(model, testloader):
    model.eval()
    total = 0

    test_mse_loss = 0
    test_crossentropy_loss = 0
    accuracy_metric = Accuracy()
    with torch.no_grad():
        for data, teacher_features, labels_categorical in tqdm(testloader):
            teacher_features = teacher_features.cuda(non_blocking=True)
            labels_categorical = labels_categorical.cuda(non_blocking=True)
            data = data.cuda(non_blocking=True)

            total += labels_categorical.size(0)

            lenet_features, lenet_predictions = model(data)

            test_mse_loss += F.mse_loss(lenet_features, teacher_features, reduction='sum').item()
            test_crossentropy_loss += F.cross_entropy(lenet_predictions, labels_categorical, reduction='sum').item()

            accuracy_metric.update((lenet_predictions, labels_categorical))

    test_mse_loss /= total
    test_crossentropy_loss /= total
    accuracy = accuracy_metric.compute()
    print(
        f'Test MSE(reduction sum): {test_mse_loss}, Crossentropy(reduction sum): {test_crossentropy_loss}, Accuracy({accuracy_metric._type}): {accuracy}')


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    # sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
    #     args.percentage_used) + '.txt'))
    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        print(f"Learning rate optimizer {args.lr}  Batch-size {args.batch_size}")
    else:
        print("Currently using CPU")
        exit()


if __name__ == '__main__':
    main()
