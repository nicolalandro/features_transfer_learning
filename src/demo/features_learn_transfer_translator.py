import argparse
import datetime
import os
import time

import torch
import torch.nn as nn
import torch.nn.functional as F
from PIL import Image
from ignite.metrics import Accuracy
from torch.optim import lr_scheduler
from torchvision import transforms
from tqdm import tqdm

from src.custom_datasets.image_feature_folder import ImageAndFeatureFolder
from src.custom_models.inception import inception_v3
from src.custom_models.lenet5 import LeNet
from src.custom_models.resnet_multiteacher import resnet50
from src.demo.mobilenet import mobilenetv3_mod, mobilenetv3
from src.demo.translators.translator_autoencoder import TranslatorAutoencoder, TranslatorClassifier
from src.utils import AverageMeter

parser = argparse.ArgumentParser("Transfer Knowledge from resnet 50")

# dataset
parser.add_argument('-d', '--dataset', type=str, default='cub200',
                    choices=['cub200'])
parser.add_argument('--path', type=str, default='/media/mint/Barracuda/Datasets/cifar10/')
parser.add_argument('--features-path', type=str,
                    default='/media/mint/Barracuda/Datasets/cifar10/resnet50_imagenet-features/')

# optimization
parser.add_argument('--batch-size', type=int, default=128)
parser.add_argument('--lr', type=float, default=0.001, help="learning rate for model")
parser.add_argument('--stepsize', type=int, default=200)
parser.add_argument('--gamma', type=float, default=0.1, help="learning rate decay")

# model
parser.add_argument('--teacher', type=str, default='ntsnet',
                    choices=['ntsnet', 'none', ])
parser.add_argument('--translator-type', type=str, default='classifier',
                    choices=['simple', 'autoencoder', 'classifier', 'autoencoder_classifier'])
parser.add_argument('--simple-model', type=str, default='inceptionv3',
                    choices=['mobilenetv3orig', 'mobilenetv3', 'lenet', 'resnet50', 'inceptionv3'])
parser.add_argument('--max-epoch', type=int, default=200)
parser.add_argument('--when_save_model', type=int, default='201', help="save model at epoch x")
parser.add_argument('--save-dir', type=str,
                    default='/media/mint/Barracuda/Models/CIFAR_10/resnet50_withresnet50teacher')

# pythorch seed
parser.add_argument('--seed', type=int, default=1, help='using a fixed seed to be reproducible')

# selected gpus
parser.add_argument('--gpu', type=str, default='0', help='select the gpu or gpus to use')

args = parser.parse_args()


def main():
    torch.manual_seed(args.seed)

    print('--- START Arg parse ---')
    options = vars(args)
    for k, v in options.items():
        print(k, ':', v)
    print('--- END Arg parse ---')

    test_gpu_settings()

    image_shape = (32, 32)
    transform_train = transforms.Compose([
        transforms.Resize(image_shape, Image.BILINEAR),
        # transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
    ])
    trainset = ImageAndFeatureFolder(root=os.path.join(args.path, 'train'),
                                     features_root=args.features_path,
                                     transform=transform_train)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True, num_workers=2)

    transform_test = transforms.Compose([
        transforms.Resize(image_shape, Image.BILINEAR),
        transforms.ToTensor(),
    ])
    testset = ImageAndFeatureFolder(root=os.path.join(args.path, 'test'),
                                    features_root=args.features_path,
                                    transform=transform_test)
    testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=2)

    num_classes = len(trainset.classes)
    print(trainset.classes)

    features_size = 64
    if args.simple_model == 'mobilenetv3orig':
        features_size = 960
    elif args.simple_model == 'resnet50':
        features_size = 2048
    elif args.simple_model == 'inceptionv3':
        features_size = 2048

    model = create_model(args.simple_model, features_size, num_classes)

    criterion_mse = nn.MSELoss().cuda()
    criterion_xent = nn.CrossEntropyLoss().cuda()

    params = list(model.parameters())
    optimizer = torch.optim.Adam(params, args.lr)

    teacher_features_size = 2048  # 10240
    if args.translator_type == 'simple':
        translator = nn.Sequential(
            nn.Linear(teacher_features_size, features_size)
        )
    elif args.translator_type == 'autoencoder':
        translator = TranslatorAutoencoder(input_size=teacher_features_size, intermediate_size=features_size)
        criterion_translator = nn.MSELoss().cuda()
    elif args.translator_type == 'classifier':
        translator = TranslatorClassifier(input_size=teacher_features_size, intermediate_size=features_size)
        translator.dense3 = model.classifier
        criterion_translator = nn.CrossEntropyLoss().cuda()

    if args.translator_type == 'classifier':
        translator_params = list(translator.translator_only_paramiters())
    else:
        translator_params = list(translator.parameters())
    translator = nn.DataParallel(translator).cuda()
    model = nn.DataParallel(model).cuda()
    optimizer_translator = torch.optim.Adam(translator_params, args.lr)

    # scheduler = lr_scheduler.StepLR(optimizer, step_size=args.stepsize, gamma=args.gamma)
    scheduler = lr_scheduler.CosineAnnealingLR(optimizer, args.max_epoch, 0.0001)
    scheduler_translator = lr_scheduler.CosineAnnealingLR(optimizer_translator, args.max_epoch, 0.0001)

    start_time = time.time()
    for epoch in range(args.max_epoch):
        print("==> Start Epoch {}/{}".format(epoch + 1, args.max_epoch))
        train_model(model, translator, trainloader, criterion_mse, criterion_xent, optimizer, criterion_translator,
                    optimizer_translator, epoch)

        scheduler.step()
        scheduler_translator.step()

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

        test(model, translator, testloader)

        if (epoch + 1) % args.when_save_model == 0:  # Save trained model
            print("Model trained saveing...")
            torch.save(model,
                       f'{args.save_dir}/{args.simple_model}_{args.teacher}_{epoch + 1}.pt')
        print("==> End Epoch {}/{}".format(epoch + 1, args.max_epoch))


def create_model(model_name, features_size, num_classes):
    if model_name == 'mobilenetv3':
        model = mobilenetv3_mod.mobilenetv3_large(**{'num_classes': num_classes, 'features_size': features_size, })
    elif model_name == 'mobilenetv3orig':
        model = mobilenetv3.mobilenetv3_large(**{'num_classes': num_classes, })
    elif model_name == 'lenet':
        model = LeNet(num_classes=num_classes, features_size=features_size)
    elif model_name == 'resnet50':
        model = resnet50(**{'num_classes': num_classes, })
    elif model_name =='inceptionv3':
        model = inception_v3(**{'num_classes': num_classes, })
    return model


def train_model(model, translator, trainloader, criterion_mse, criterion_xent, optimizer, criterion_translator,
                optimizer_translator, epoch):
    model.train()
    translator.train()
    mse_losses = AverageMeter()
    xent_losses = AverageMeter()
    translator_losses = AverageMeter()

    for batch_idx, (data, features, labels) in enumerate(trainloader):
        features = features.cuda(non_blocking=True)
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        if args.translator_type == 'simple':
            translated_features = translator(features)
        elif args.translator_type == 'autoencoder':
            translated_features, decoded = translator(features)
        elif args.translator_type == 'classifier':
            translated_features, translator_classifier = translator(features)

        lenet_features, lenet_predictions = model(data)

        loss_mse = criterion_mse(lenet_features, translated_features)
        loss_xent = criterion_xent(lenet_predictions, labels)

        if args.teacher == 'none':
            loss = loss_xent
        elif epoch < -1:
            loss = loss_mse
        else:
            loss = loss_mse + loss_xent

        if args.translator_type == 'autoencoder':
            loss_translator = criterion_translator(decoded, features)
            loss += loss_translator
            translator_losses.update(loss_translator.item(), labels.size(0))
        elif args.translator_type == 'classifier':
            loss_translator = criterion_translator(translator_classifier, labels)
            loss += loss_translator
            translator_losses.update(loss_translator.item(), labels.size(0))

        optimizer.zero_grad()
        optimizer_translator.zero_grad()
        loss.backward()

        optimizer.step()
        optimizer_translator.step()

        mse_losses.update(loss_mse.item(), labels.size(0))
        xent_losses.update(loss_xent.item(), labels.size(0))

        if (batch_idx + 1) % 20 == 0:
            print(f"Batch {batch_idx + 1}\t",
                  f" MSE {mse_losses.val} ({mse_losses.avg})",
                  f", CrossEntropy {xent_losses.val} ({xent_losses.avg})",
                  f", TransLoss {translator_losses.val} ({translator_losses.avg})",
                  )


def test(model, translator, testloader):
    model.eval()
    translator.eval()
    total = 0

    test_mse_loss = 0
    test_crossentropy_loss = 0
    test_translator_loss = 0
    accuracy_metric = Accuracy()
    with torch.no_grad():
        for data, teacher_features, labels_categorical in tqdm(testloader):
            teacher_features = teacher_features.cuda(non_blocking=True)
            labels_categorical = labels_categorical.cuda(non_blocking=True)
            data = data.cuda(non_blocking=True)

            total += labels_categorical.size(0)

            if args.translator_type == 'simple':
                translated_features = translator(teacher_features)
            elif args.translator_type == 'autoencoder':
                translated_features, decoded = translator(teacher_features)
            elif args.translator_type == 'classifier':
                translated_features, translator_classifier = translator(teacher_features)
            lenet_features, lenet_predictions = model(data)

            test_mse_loss += F.mse_loss(lenet_features, translated_features, reduction='sum').item()
            test_crossentropy_loss += F.cross_entropy(lenet_predictions, labels_categorical, reduction='sum').item()
            if args.translator_type == 'autoencoder':
                test_translator_loss += F.mse_loss(decoded, teacher_features, reduction='sum').item()
            elif args.translator_type == 'classifier':
                test_translator_loss += F.cross_entropy(translator_classifier, labels_categorical,
                                                        reduction='sum').item()

            accuracy_metric.update((lenet_predictions, labels_categorical))

    test_mse_loss /= total
    test_crossentropy_loss /= total
    test_translator_loss /= total
    accuracy = accuracy_metric.compute()
    print(f'Test MSE(reduction sum): {test_mse_loss},'
          f', Crossentropy(reduction sum): {test_crossentropy_loss}',
          f', TransLoss(reduction sum): {test_translator_loss}',
          f', Accuracy({accuracy_metric._type}): {accuracy}',
          )


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    # sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
    #     args.percentage_used) + '.txt'))
    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        print(f"Learning rate optimizer {args.lr}  Batch-size {args.batch_size}")
    else:
        print("Currently using CPU")
        exit()


if __name__ == '__main__':
    main()
