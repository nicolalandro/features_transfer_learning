import argparse
import datetime
import os
import time

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
from PIL import Image
from ignite.metrics import Accuracy
from torch.optim import lr_scheduler
from torchvision import transforms

from cifar10_models import inception_v3, resnet50
from src.custom_models.simplified_inceptionv3 import inception_simplified
from src.demo.inception.inceptionv4 import Inceptionv4
from src.demo.mobilenet.mobilenetv2 import mobilenet_v2
from src.demo.mobilenet.mobilenetv3 import mobilenetv3_large
from src.demo.nts_net import resnet
from src.demo.tresnet.tresnet import TResNet
from src.utils import AverageMeter

parser = argparse.ArgumentParser("Loretta dogs mobilenetv2")

# dataset
parser.add_argument('-d', '--dataset', type=str, default='LorettaDogs',
                    choices=['LorettaDogs'])
parser.add_argument('--path', type=str, default='/media/mint/Barracuda/Datasets/cifar100/')

# optimization
parser.add_argument('--batch-size', type=int, default=128)
parser.add_argument('--lr', type=float, default=0.001, help="learning rate for model")
parser.add_argument('--stepsize', type=int, default=200)
parser.add_argument('--gamma', type=float, default=0.1, help="learning rate decay")

# model
parser.add_argument('--simple-model', type=str, default='inceptionCIFAR',
                    choices=['tresnet', 'mobilenetV2', 'torch_vision_mobilenetV2', 'mobilenetv3_large',
                             'resnet50_pretrained', 'resnet50CIFAR',
                             'inceptionV4', 'inceptionCIFAR', 'inceptionSIMPLY'])
parser.add_argument('--max-epoch', type=int, default=50)
parser.add_argument('--when_save_model', type=int, default=1, help="save model at epoch x")
parser.add_argument('--save-dir', type=str,
                    default='/media/mint/Barracuda/Models/CIFAR_100/resnet50')

# pythorch seed
parser.add_argument('--seed', type=int, default=1, help='using a fixed seed to be reproducible')

# selected gpus
parser.add_argument('--gpu', type=str, default='0', help='select the gpu or gpus to use')

args = parser.parse_args()

ONE_OUTPUT_MODELS = ['torch_vision_mobilenetV2', 'tresnet']


def main():
    torch.manual_seed(args.seed)

    # args.simple_model = 'resnet50_cifar100'
    print('--- START Arg parse ---')
    options = vars(args)
    for k, v in options.items():
        print(k, ':', v)
    print('--- END Arg parse ---')

    test_gpu_settings()

    img_shape = (32, 32)
    transform_train = transforms.Compose([
        transforms.Resize(img_shape, Image.BILINEAR),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
    ])
    train_path = os.path.join(args.path, 'train')
    trainset = torchvision.datasets.ImageFolder(root=train_path, transform=transform_train)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True, num_workers=2)

    transform_test = transforms.Compose([
        transforms.Resize(img_shape, Image.BILINEAR),
        transforms.ToTensor(),
    ])
    test_path = os.path.join(args.path, 'test')
    testset = torchvision.datasets.ImageFolder(root=test_path, transform=transform_test)
    testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=2)

    classes_dict = {i: v for i, v in enumerate(trainset.classes)}
    num_classes = len(classes_dict)

    if args.simple_model == 'torch_vision_mobilenetV2':
        model = torchvision.models.mobilenet_v2(pretrained=False,
                                                **{'num_classes': num_classes,
                                                   'round_nearest': 8})
    elif args.simple_model == 'mobilenetV2':
        features_size = 10400
        model = mobilenet_v2(pretrained=False,
                             **{'num_classes': num_classes,
                                'features_size': features_size,
                                'round_nearest': 8})
    elif args.simple_model == 'mobilenetv3_large':
        model = mobilenetv3_large(**{'num_classes': num_classes})
    elif args.simple_model == 'resnet50_pretrained':
        model = resnet.resnet50(pretrained=True)
        model.avgpool = nn.AdaptiveAvgPool2d(1)
        model.fc = nn.Linear(512 * 4, num_classes)
        state_dict = torch.load(
            '/media/mint/Barracuda/Datasets/LorettaDogs/ModelsFeatureTransfer/resnet50/0_starter_resnet50.pt')
        # print(state_dict.keys())
        state_dict_new = {k[7:]: state_dict[k] for k in state_dict.keys()}
        model.load_state_dict(
            state_dict_new)
    elif args.simple_model == 'inceptionV4':
        model = Inceptionv4(classes=num_classes)
    elif args.simple_model == 'tresnet':
        model = TResNet(layers=[4, 5, 24, 3], num_classes=num_classes, in_chans=3, width_factor=1.3,
                        remove_aa_jit=False)
    elif args.simple_model == 'inceptionCIFAR':
        model = inception_v3(num_classes=num_classes)
    elif args.simple_model == 'resnet50CIFAR':
        model = resnet50(pretrained=True, num_classes=10)
        model.fc = nn.Linear(512 * 4, num_classes)
    elif args.simple_model == 'inceptionSIMPLY':
        model = inception_simplified.inception_v3()

    model = nn.DataParallel(model).cuda()

    criterion_xent = nn.CrossEntropyLoss().cuda()

    params = list(model.parameters())
    # optimizer = torch.optim.Adam(params, args.lr)
    optimizer = torch.optim.SGD(params, 0.008)
    # optimizer = torch.optim.RMSprop(params, lr=args.lr, alpha=0.99, eps=1e-8, momentum=0,
    #                                 centered=False)

    # scheduler = lr_scheduler.StepLR(optimizer, step_size=args.stepsize, gamma=args.gamma)
    scheduler = lr_scheduler.CosineAnnealingLR(optimizer, args.max_epoch, 0.0008)

    start_time = time.time()
    for epoch in range(args.max_epoch):
        print("==> Start Epoch {}/{}".format(epoch + 1, args.max_epoch))
        train_model(model, trainloader, criterion_xent, optimizer)

        scheduler.step()

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

        test(model, testloader)

        if (epoch + 1) % args.when_save_model == 0:  # Save trained model
            print("Model trained saveing...")
            torch.save(model,
                       f'{args.save_dir}/lorettadogs_{args.simple_model}_{epoch + 1}.pt')
        print("==> End Epoch {}/{}".format(epoch + 1, args.max_epoch))


def train_model(model, trainloader, criterion_xent, optimizer):
    model.train()
    xent_losses = AverageMeter()

    for batch_idx, (data, labels) in enumerate(trainloader):
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)
        if args.simple_model in ONE_OUTPUT_MODELS:
            predictions = model(data)
        elif args.simple_model == 'resnet50_pretrained':
            predictions, _, _ = model(data)
        else:
            _, predictions = model(data)
        loss_xent = criterion_xent(predictions, labels)

        # loss = loss_xent
        loss = loss_xent

        optimizer.zero_grad()
        loss.backward()

        optimizer.step()

        xent_losses.update(loss_xent.item(), labels.size(0))

        if (batch_idx + 1) % 20 == 0:
            print(
                f"Batch {batch_idx + 1}\t CrossEntropy {xent_losses.val} ({xent_losses.avg})")


def test(model, testloader):
    model.eval()
    total = 0

    test_crossentropy_loss = 0
    accuracy_metric = Accuracy()
    with torch.no_grad():
        for data, labels_categorical in testloader:
            labels_categorical = labels_categorical.cuda(non_blocking=True)
            data = data.cuda(non_blocking=True)

            total += labels_categorical.size(0)

            if args.simple_model in ONE_OUTPUT_MODELS:
                lenet_predictions = model(data)
            elif args.simple_model == 'resnet50_pretrained':
                lenet_predictions, _, _ = model(data)
            else:
                _, lenet_predictions = model(data)

            test_crossentropy_loss += F.cross_entropy(lenet_predictions, labels_categorical, reduction='sum').item()

            accuracy_metric.update((lenet_predictions, labels_categorical))

    test_crossentropy_loss /= total
    accuracy = accuracy_metric.compute()
    print(
        f'Test Crossentropy(reduction sum): {test_crossentropy_loss}, Accuracy({accuracy_metric._type}): {accuracy}')


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    # sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
    #     args.percentage_used) + '.txt'))
    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        print(f"Learning rate optimizer {args.lr}  Batch-size {args.batch_size}")
    else:
        print("Currently using CPU")
        exit()


if __name__ == '__main__':
    main()
