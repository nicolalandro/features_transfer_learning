from ptflops import get_model_complexity_info
from torch import nn

from src.demo.mobilenet import mobilenetv3_two_teacher
from src.demo.nts_net.model import attention_net, resnet
from src.custom_models.lenet5 import LeNet
from src.demo.tresnet.tresnet import TResNet

num_classes = 131


def create_resnet50(n_class):
    net = resnet.resnet50(pretrained=False)
    net.avgpool = nn.AdaptiveAvgPool2d(1)
    net.fc = nn.Linear(512 * 4, n_class)
    return net


models = {
    'lenet': LeNet(num_classes=num_classes),
    'mobilenetv3-small': mobilenetv3_two_teacher.mobilenetv3_small(**{'num_classes': num_classes, }),
    'mobilenetv3-large': mobilenetv3_two_teacher.mobilenetv3_large(**{'num_classes': num_classes, }),
    'resnet50': create_resnet50(num_classes),
    'tresnet': TResNet(layers=[4, 5, 24, 3], num_classes=num_classes, in_chans=3, width_factor=1.3,
                       remove_aa_jit=False),
    'nts-net': attention_net(topN=6, num_classes=num_classes, device='cpu'),
}

for k, model in models.items():
    params = sum(p.numel() for p in model.parameters())

    for sample in [(3, 32, 32), (3, 224, 224), (3, 448, 448)]:
        try:
            macs, _ = get_model_complexity_info(
                model,
                sample,
                as_strings=False,
                print_per_layer_stat=False,
                verbose=False,
            )
            break
        except:
            pass
    print(k)
    print('\tparams:', f'{params:.3E}')
    print('\tflops:', f'{macs:.3E}')
