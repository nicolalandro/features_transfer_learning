import time


class Timeing:
    def __init__(self, msg):
        self.msg = msg
        self.start_time = time.time()

    def __enter__(self):
        print(self.msg)

    def __exit__(self, exc_type, exc_val, exc_tb):
        elapsed = round(time.time() - self.start_time)
        print(f'...time: {elapsed}')
