import torch

from torchvision.models import resnet50 as m
from thop import profile
from ptflops import get_model_complexity_info

num_classes = 131
model = m(**{'num_classes': num_classes, })
random_input = torch.randn(1, 3, 224, 224)

print('thop')
macs, params = profile(model, inputs=(random_input,))
print('\tGflops:', macs/1000000, ', Milions of params:', params/1000000)

print('ptflops')
macs, params = get_model_complexity_info(model, (3, 224, 224), as_strings=True,
                                         print_per_layer_stat=True, verbose=True)
print('\tflops:', macs, ', params:', params)
