import os

import torch
import torch.nn.functional as F
import torchvision
from PIL import Image
from ignite.metrics import Accuracy
from torch import nn
from torchvision import transforms
from tqdm import tqdm

from src.demo.nts_net import model

dataset_path = '/media/mint/Caddy/Datasets/LorettaDogs/'
environment = 'test'

device = 'cuda'

net = model.attention_net(topN=6, num_classes=131, device=device)
ckpt = torch.load('/media/mint/Caddy/Datasets/LorettaDogs/ModelsFeatureTransfer/20200409_164553/088.ckpt')
net.load_state_dict(ckpt['net_state_dict'])
print('nts-net test_acc:', ckpt['test_acc'])

resnet50 = net.pretrained_model

del net

resnet50 = nn.DataParallel(resnet50).cuda()

batch_size = 200
transform_test = transforms.Compose([
    transforms.Resize((224, 224), Image.BILINEAR),
    transforms.ToTensor(),
])
test_path = os.path.join(dataset_path, environment)
testset = torchvision.datasets.ImageFolder(root=test_path, transform=transform_test)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=2)

resnet50.eval()

total = 0

test_crossentropy_loss = 0
accuracy_metric = Accuracy()
with torch.no_grad():
    for data, labels_categorical in tqdm(testloader):
        labels_categorical = labels_categorical.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        total += labels_categorical.size(0)

        predictions, _, features = resnet50(data)

        test_crossentropy_loss += F.cross_entropy(predictions, labels_categorical, reduction='sum').item()

        accuracy_metric.update((predictions, labels_categorical))

test_crossentropy_loss /= total
accuracy = accuracy_metric.compute()
print(
    f'Test Crossentropy(reduction sum): {test_crossentropy_loss}',
    f', Accuracy({accuracy_metric._type}): {accuracy}')

resnet50.to('cpu')
torch.save(resnet50.state_dict(),
           '/media/mint/Caddy/Datasets/LorettaDogs/ModelsFeatureTransfer/resnet50/0_starter_resnet50.pt')