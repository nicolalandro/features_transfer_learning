import os

import torch
import torchvision
from PIL import Image
from torch import nn
from torchvision import transforms
from tqdm import tqdm

from src.custom_datasets.read_image_and_name import ImageAndNameFolder
from src.custom_models.nts_net.model import ntsnet

use_gpu = torch.cuda.is_available()

batch_size = 20
dataset_path = '/media/mint/Barracuda/Datasets/cifar100/'
output_path = '/media/mint/Barracuda/Datasets/cifar100/resnet50-outputs-cpu'
# model_path = '/media/mint/Barracuda/Models/CIFAR_100/inceptionv3/lorettadogs_inceptionCIFAR_199.pt'
# model_path = '/media/mint/Barracuda/Models/CIFAR_10/resnet50/lorettadogs_resnet50CIFAR_400.pt'
model_path = '/media/mint/Barracuda/Models/CIFAR_100/resnet50/lorettadogs_resnet50CIFAR_400.pt'
environment = 'test'

img_shape = (32, 32)
transform_test = transforms.Compose([
    transforms.Resize(img_shape, Image.BILINEAR),
    transforms.ToTensor(),
])
testset = ImageAndNameFolder(root=os.path.join(dataset_path, environment), transform=transform_test)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=2)

net = torch.load(model_path)
net = nn.DataParallel(net).cuda()

net.eval()

with torch.no_grad():
    for data, img_names in tqdm(testloader):
        features, outputs = net(data)

        for img, file_name in zip(outputs, img_names):
            splitted_filename = file_name.split('/')
            folder_path = os.path.join(output_path, environment, splitted_filename[-2])
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)
            file_output_path = os.path.join(folder_path, splitted_filename[-1] + '.pt')
            torch.save(img.to('cpu'), file_output_path)
