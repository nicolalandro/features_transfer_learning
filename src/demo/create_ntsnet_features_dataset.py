import os

import torch
from PIL import Image
from torch import nn
from torchvision import transforms
from tqdm import tqdm

from src.custom_datasets.read_image_and_name import ImageAndNameFolder
from src.demo.nts_net import model

use_gpu = torch.cuda.is_available()

output_path = '/media/mint/Barracuda/Datasets/cifar10/nts-net-lorettadogs-features'
dataset_path = '/media/mint/Barracuda/Datasets/cifar10/'
environment = 'test'

transform_test = transforms.Compose([
    transforms.Resize((448, 448), Image.BILINEAR),
    transforms.ToTensor(),
])
testset = ImageAndNameFolder(root=dataset_path + environment, transform=transform_test)
testloader = torch.utils.data.DataLoader(testset, batch_size=60, shuffle=False, num_workers=2)

device = 'cuda'
net = model.attention_net(topN=6, num_classes=131, device=device)
ckpt = torch.load('/media/mint/Barracuda/Datasets/LorettaDogs/ModelsFeatureTransfer/20200409_164553/088.ckpt')
net.load_state_dict(ckpt['net_state_dict'])
print('test_acc:', ckpt['test_acc'])
net = nn.DataParallel(net).cuda()

net.eval()

with torch.no_grad():
    for data, img_names in tqdm(testloader):
        data = data.to(device)
        top_n_coordinates, concat_out, raw_logits, concat_logits, part_logits, top_n_index, top_n_prob = net(data)

        for img, file_name in zip(concat_out, img_names):
            splitted_filename = file_name.split('/')
            folder_path = os.path.join(output_path, environment, splitted_filename[-2])
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)
            file_output_path = os.path.join(folder_path, splitted_filename[-1] + '.pt')
            torch.save(img.to('cpu'), file_output_path)
