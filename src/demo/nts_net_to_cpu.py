import torch

from src.demo.nts_net.model import attention_net

if __name__ == '__main__':
    PROPOSAL_NUM = 6
    numclasses = 131
    device = torch.device('cpu')
    net = attention_net(topN=PROPOSAL_NUM, num_classes=numclasses, device=device)
    ckpt = torch.load('/media/mint/Caddy/Datasets/LorettaDogs/ModelsFeatureTransfer/20200409_164553/088.ckpt')
    net.load_state_dict(ckpt['net_state_dict'])
    print('test_acc:', ckpt['test_acc'])

    net.to(device)
    torch.save(net.state_dict(), 'models/nts_net_state.pt', _use_new_zipfile_serialization=True)
