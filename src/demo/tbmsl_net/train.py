# coding=utf-8
import torch
import torch.nn as nn
import torchvision
from PIL import Image
from torch.optim.lr_scheduler import MultiStepLR
import shutil
import time

from torchvision import transforms

from src.demo.tbmsl_net.config import num_classes, model_name, model_path, lr_milestones, lr_decay_rate, input_size, \
    root, end_epoch, save_interval, init_lr, batch_size, CUDA_VISIBLE_DEVICES, weight_decay, \
    proposalN, set, channels
from src.demo.tbmsl_net.utils.train_model import train
from src.demo.tbmsl_net.utils.auto_laod_resume import auto_load_resume
from src.demo.tbmsl_net.model import MainNet

import os

os.environ['CUDA_VISIBLE_DEVICES'] = CUDA_VISIBLE_DEVICES


def main():
    trainloader, testloader = read_dataset(input_size, batch_size, root)

    model = MainNet(proposalN=proposalN, num_classes=num_classes, channels=channels)

    criterion = nn.CrossEntropyLoss()

    parameters = model.parameters()

    save_path = os.path.join(model_path, model_name)
    if os.path.exists(save_path):
        start_epoch, lr = auto_load_resume(model, save_path, status='train')
        assert start_epoch < end_epoch
    else:
        os.makedirs(save_path)
        start_epoch = 0
        lr = init_lr

    # define optimizers
    optimizer = torch.optim.SGD(parameters, lr=lr, momentum=0.9, weight_decay=weight_decay)

    model = model.cuda()

    scheduler = MultiStepLR(optimizer, milestones=lr_milestones, gamma=lr_decay_rate)

    time_str = time.strftime("%Y%m%d-%H%M%S")
    shutil.copy('./config.py', os.path.join(save_path, "{}config.py".format(time_str)))

    train(model=model,
          trainloader=trainloader,
          testloader=testloader,
          criterion=criterion,
          optimizer=optimizer,
          scheduler=scheduler,
          save_path=save_path,
          start_epoch=start_epoch,
          end_epoch=end_epoch,
          save_interval=save_interval)


def read_dataset(input_size, batch_size, root):
    # input_size = 448
    transform_train = transforms.Compose([
        transforms.Resize((input_size, input_size), Image.BILINEAR),
        transforms.RandomHorizontalFlip(),  # solo se train
        transforms.ToTensor(),
    ])

    transform_test = transforms.Compose([
        transforms.Resize((input_size, input_size), Image.BILINEAR),
        transforms.ToTensor(),
    ])

    train_path = os.path.join(root, 'train')
    trainset = torchvision.datasets.ImageFolder(root=train_path, transform=transform_train)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True,
                                              num_workers=2)

    test_path = os.path.join(root, 'test')
    testset = torchvision.datasets.ImageFolder(root=test_path, transform=transform_test)
    testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=2)

    return trainloader, testloader

if __name__ == '__main__':
    main()
