import torch

# mobilenetv3_transfer.log accuracy: 0.5653375444137386 (at epoch 185) epochs: 200
# mobilenetv3_notransfer.log accuracy: 0.38136596920647453 (at epoch 152) epochs: 165
# mobilenetv3_original.log accuracy: 0.4660481642321358 (at epoch 172) epochs: 200
# nts-net.log accuracy: 0.824 (at epoch 104) epochs: 200

if __name__ == '__main__':
    device = torch.device('cpu')
    # folder = 'mobilenet_transfer'
    # model = 'mobilenetv3_ntsnet_185.pt'
    folder = 'mobilenet_no_trainsfer'
    model = 'lorettadogs_mobilenetv3_large_172.pt'
    net = torch.load(f'models/{folder}/{model}')

    net.to(device)
    torch.save(net.state_dict(), 'models/mobilenetv3_large.pt', _use_new_zipfile_serialization=True)
