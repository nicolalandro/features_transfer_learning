import argparse
import datetime
import os
import time

import torch
import torch.nn as nn
import torch.nn.functional as F
from PIL import Image
from ignite.metrics import Accuracy
from torch.optim import lr_scheduler
from torchvision import transforms
from tqdm import tqdm

try:
    import src
except:
    import sys
    print(os.listdir())
    sys.path.insert(0, './')


from src.custom_datasets.image_feature_folder import ImageAnd2FeatureFolder
from src.custom_models.inception import inception_v3
from src.custom_models.lenet5 import LeNet3Output
from src.demo.mobilenet import mobilenetv3_mod, mobilenetv3, mobilenetv3_two_teacher
from src.demo.translators.translator_autoencoder import MultiTranslatorClassifier, MultiTranslatorClassifierConcat, \
    TwoTranslator, TwoTranslatorClassifier
from src.utils import AverageMeter

parser = argparse.ArgumentParser("Transfer Knowledge from resnet 50")

# dataset
parser.add_argument('-d', '--dataset', type=str, default='cifar10',
                    choices=['cub200', 'cifar10'])
parser.add_argument('--path', type=str, default='/home/super/datasets-nas/cifar_10_features/')
parser.add_argument('--features-path1', type=str,
                    default='/home/super/datasets-nas/cifar_10_features/nts-net-lorettadogs-features/')
parser.add_argument('--features-path2', type=str,
                    default='/home/super/datasets-nas/cifar_10_features/resnet50_imagenet-features/')
# optimization
parser.add_argument('--batch-size', type=int, default=32)
parser.add_argument('--lr', type=float, default=0.001, help="learning rate for model")
parser.add_argument('--stepsize', type=int, default=100)
parser.add_argument('--gamma', type=float, default=0.1, help="learning rate decay")

# model
parser.add_argument('--teacher', type=str, default='ntsnet',
                    choices=['ntsnet', 'none', ])
parser.add_argument('--translator-type', type=str, default='two_translator_classifier',
                    choices=['classifier', 'classifier_concat', 'concat', 'two_translator',
                             'two_translator_classifier'])
parser.add_argument('--simple-model', type=str, default='inceptionv3',
                    choices=['mobilenetv3orig', 'mobilenetv3', 'mobilenetv3tt', 'lenet', 'inceptionv3'])
parser.add_argument('--max-epoch', type=int, default=200)
parser.add_argument('--when_save_model', type=int, default='201', help="save model at epoch x")
parser.add_argument('--save-dir', type=str,
                    default='/home/super/Models/inceptionv3')

# pythorch seed
parser.add_argument('--seed', type=int, default=1, help='using a fixed seed to be reproducible')

# selected gpus
parser.add_argument('--gpu', type=str, default='0', help='select the gpu or gpus to use')

args = parser.parse_args()

threeoutputs = ['mobilenetv3tt', 'lenet', 'inceptionv3']


def main():
    torch.manual_seed(args.seed)

    print('--- START Arg parse ---')
    options = vars(args)
    for k, v in options.items():
        print(k, ':', v)
    print('--- END Arg parse ---')

    test_gpu_settings()

    image_shape = (32, 32)
    transform_train = transforms.Compose([
        transforms.Resize(image_shape, Image.BILINEAR),
        # transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
    ])
    trainset = ImageAnd2FeatureFolder(root=os.path.join(args.path, 'train'),
                                      features_root1=args.features_path1,
                                      features_root2=args.features_path2,
                                      transform=transform_train)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True, num_workers=2)

    transform_test = transforms.Compose([
        transforms.Resize(image_shape, Image.BILINEAR),
        transforms.ToTensor(),
    ])
    testset = ImageAnd2FeatureFolder(root=os.path.join(args.path, 'test'),
                                     features_root1=args.features_path1,
                                     features_root2=args.features_path2,
                                     transform=transform_test)
    testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=2)

    num_classes = len(trainset.classes)
    print(trainset.classes)
    features_size = 64
    second_features_size = 120
    if args.simple_model == 'mobilenetv3tt':
        features_size = 1280
        second_features_size = 960
    elif args.simple_model == 'inceptionv3':
        features_size = 100352
        second_features_size = 2048

    model = create_model(args.simple_model, features_size, num_classes)

    criterion_mse = nn.MSELoss().cuda()
    criterion_xent = nn.CrossEntropyLoss().cuda()

    params = list(model.parameters())
    optimizer = torch.optim.Adam(params, args.lr)

    if args.translator_type == 'classifier':
        translator = MultiTranslatorClassifier()
        translator.dense3 = model.classifier
        criterion_translator = nn.CrossEntropyLoss().cuda()
        translator_params = list(translator.translator_only_paramiters())
    elif args.translator_type == 'classifier_concat':
        translator = MultiTranslatorClassifierConcat()
        translator.dense3 = model.cllassifier
        criterion_translator = nn.CrossEntropyLoss().cuda()
        translator_params = list(translator.translator_only_paramiters())
    elif args.translator_type == 'concat':
        translator = MultiTranslatorClassifierConcat()
        criterion_translator = nn.CrossEntropyLoss().cuda()
        translator_params = list(translator.translator_only_paramiters())
    elif args.translator_type == 'two_translator':
        translator = TwoTranslator()
        translator_params = list(translator.parameters())
        criterion_translator = None
    elif args.translator_type == 'two_translator_classifier':
        translator = TwoTranslatorClassifier(input_size1=10240, input_size2=2048,
                                             intermediate_size2=second_features_size, intermediate_size1=features_size,
                                             inceptionv3=(args.simple_model == 'inceptionv3'))

        if args.simple_model == 'mobilenetv3tt' or args.simple_model == 'inceptionv3':
            translator.intermediate = model.before_classifier
            translator.classifier = model.classifier
        else:
            translator.intermediate = model.before_classifier()
            translator.classifier = model.classifier()

        criterion_translator = nn.CrossEntropyLoss().cuda()
        translator_params = list(translator.translator_only_paramiters())

    translator = nn.DataParallel(translator).cuda()
    model = nn.DataParallel(model).cuda()
    optimizer_translator = torch.optim.Adam(translator_params, args.lr)

    # scheduler = lr_scheduler.StepLR(optimizer, step_size=args.stepsize, gamma=args.gamma)
    scheduler = lr_scheduler.CosineAnnealingLR(optimizer, args.max_epoch, 0.0001)
    scheduler_translator = lr_scheduler.CosineAnnealingLR(optimizer_translator, args.max_epoch, 0.0001)

    start_time = time.time()
    for epoch in range(args.max_epoch):
        print("==> Start Epoch {}/{}".format(epoch + 1, args.max_epoch))
        train_model(model, translator, trainloader, criterion_mse, criterion_xent, optimizer, criterion_translator,
                    optimizer_translator, epoch)

        scheduler.step()
        scheduler_translator.step()

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

        test(model, translator, testloader)

        if (epoch + 1) % args.when_save_model == 0:  # Save trained model
            print("Model trained saveing...")
            torch.save(model,
                       f'{args.save_dir}/{args.simple_model}_{args.teacher}_{epoch + 1}.pt')
        print("==> End Epoch {}/{}".format(epoch + 1, args.max_epoch))


def create_model(model_name, features_size, num_classes):
    if model_name == 'mobilenetv3':
        model = mobilenetv3_mod.mobilenetv3_large(**{'num_classes': num_classes, 'features_size': features_size, })
    elif model_name == 'mobilenetv3orig':
        model = mobilenetv3.mobilenetv3_large(**{'num_classes': num_classes, })
    elif model_name == 'mobilenetv3tt':
        model = mobilenetv3_two_teacher.mobilenetv3_large(**{'num_classes': num_classes, })
    elif model_name == 'lenet':
        model = LeNet3Output(num_classes=num_classes, features_size=features_size)
    elif model_name == 'inceptionv3':
        model = inception_v3(**{'num_classes': num_classes, "features_number": 2})
    return model


def train_model(model, translator, trainloader, criterion_mse, criterion_xent, optimizer, criterion_translator,
                optimizer_translator, epoch):
    model.train()
    translator.train()
    mse_losses1 = AverageMeter()
    mse_losses2 = AverageMeter()
    xent_losses = AverageMeter()
    translator_losses1 = AverageMeter()
    translator_losses2 = AverageMeter()

    for batch_idx, (data, features1, features2, labels) in enumerate(trainloader):
        features1 = features1.cuda(non_blocking=True)
        features2 = features2.cuda(non_blocking=True)
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        if args.simple_model in threeoutputs:
            features_0, features_1, predictions = model(data)
        else:
            features_0, predictions = model(data)

        if args.translator_type.startswith('classifier') or args.translator_type == 'concat':
            translated_features0, translator_classifier1 = translator(features1, features2)
        elif args.translator_type == 'two_translator':
            translated_features0, translated_features1 = translator(features1, features2)
        elif args.translator_type == 'two_translator_classifier':
            translated_features0, translated_features1, translator_classifier1, translator_classifier2 = translator(
                features1, features2)

        loss_mse = criterion_mse(features_0, translated_features0)
        loss_xent = criterion_xent(predictions, labels)

        if args.teacher == 'none':
            loss = loss_xent
        elif epoch < -1:
            loss = loss_mse
        else:
            loss = loss_mse + loss_xent

        if args.translator_type.startswith('classifier'):
            loss_translator = criterion_translator(translator_classifier1, labels)
            loss += loss_translator
            translator_losses1.update(loss_translator.item(), labels.size(0))

        if args.translator_type == 'two_translator_classifier':
            loss_translator1 = criterion_translator(translator_classifier1, labels)
            loss_translator2 = criterion_translator(translator_classifier2, labels)
            loss += loss_translator1 + loss_translator2
            translator_losses1.update(loss_translator1.item(), labels.size(0))
            translator_losses2.update(loss_translator2.item(), labels.size(0))

        if args.simple_model in threeoutputs:
            loss_mse1 = criterion_mse(features_1, translated_features1)
            loss += loss_mse1
            mse_losses2.update(loss_mse1.item(), labels.size(0))

        optimizer.zero_grad()
        optimizer_translator.zero_grad()
        loss.backward()

        optimizer.step()
        optimizer_translator.step()

        mse_losses1.update(loss_mse.item(), labels.size(0))
        xent_losses.update(loss_xent.item(), labels.size(0))

        if (batch_idx + 1) % 20 == 0:
            print(f"Batch {batch_idx + 1}\t",
                  f" MSE_1 {mse_losses1.val} ({mse_losses1.avg})",
                  f" MSE_2 {mse_losses2.val} ({mse_losses2.avg})",
                  f", CrossEntropy {xent_losses.val} ({xent_losses.avg})",
                  f", TransLoss1 {translator_losses1.val} ({translator_losses1.avg})",
                  f", TransLoss2 {translator_losses2.val} ({translator_losses2.avg})",
                  )


def test(model, translator, testloader):
    model.eval()
    translator.eval()
    total = 0

    test_mse_loss1 = 0
    test_mse_loss2 = 0
    test_crossentropy_loss = 0
    test_translator_loss1 = 0
    test_translator_loss2 = 0
    accuracy_metric = Accuracy()
    with torch.no_grad():
        for data, features1, features2, labels_categorical in tqdm(testloader):
            features1 = features1.cuda(non_blocking=True)
            features2 = features2.cuda(non_blocking=True)
            labels_categorical = labels_categorical.cuda(non_blocking=True)
            data = data.cuda(non_blocking=True)

            total += labels_categorical.size(0)

            if args.translator_type.startswith('classifier') or args.translator_type == 'concat':
                translated_features1, translator_classifier = translator(features1, features2)
            elif args.translator_type == 'two_translator':
                translated_features1, translated_features2 = translator(features1, features2)
            elif args.translator_type == 'two_translator_classifier':
                translated_features1, translated_features2, translator_classifier1, translator_classifier2 = translator(
                    features1, features2)

            if args.simple_model in threeoutputs:
                features1, features2, predictions = model(data)
            else:
                features1, predictions = model(data)

            test_mse_loss1 += F.mse_loss(features1, translated_features1, reduction='sum').item()
            test_crossentropy_loss += F.cross_entropy(predictions, labels_categorical, reduction='sum').item()
            if args.translator_type.startswith('classifier'):
                test_translator_loss1 += F.cross_entropy(translator_classifier, labels_categorical,
                                                         reduction='sum').item()
            elif args.translator_type == 'two_translator_classifier':
                test_mse_loss2 += F.mse_loss(features2, translated_features2, reduction='sum').item()
                test_translator_loss1 += F.cross_entropy(translator_classifier1, labels_categorical,
                                                         reduction='sum').item()
                test_translator_loss2 += F.cross_entropy(translator_classifier2, labels_categorical,
                                                         reduction='sum').item()

            accuracy_metric.update((predictions, labels_categorical))

    test_mse_loss1 /= total
    test_mse_loss2 /= total
    test_crossentropy_loss /= total
    test_translator_loss1 /= total
    test_translator_loss2 /= total
    accuracy = accuracy_metric.compute()
    print(f'Test MSE_1(reduction sum): {test_mse_loss1}'
          f', MSE_2(reduction sum): {test_mse_loss2}'
          f', Crossentropy(reduction sum): {test_crossentropy_loss}',
          f', TransLoss_1(reduction sum): {test_translator_loss1}',
          f', TransLoss_2(reduction sum): {test_translator_loss2}',
          f', Accuracy({accuracy_metric._type}): {accuracy}',
          )


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    # sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
    #     args.percentage_used) + '.txt'))
    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        print(f"Learning rate optimizer {args.lr}  Batch-size {args.batch_size}")
    else:
        print("Currently using CPU")
        exit()


if __name__ == '__main__':
    main()
