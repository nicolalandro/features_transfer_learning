import os

import torchvision
from torchvision import transforms

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

import torch

use_gpu = torch.cuda.is_available()

import torch.nn.functional as F
from ignite.metrics import Accuracy
from torch import nn

from src.custom_models.resnet_cifar100 import cifar_resnet56

batch_size = 128
# test_path = '/home/supreme/datasets/cifar100/cifar100/test'
# test_path = '/home/supreme/datasets/cifar10/test'
test_path = '/media/mint/Caddy/Datasets/cifar100/test'

transform_test = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
])
testset = torchvision.datasets.ImageFolder(root=test_path, transform=transform_test)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=2)

resnet_classifier = cifar_resnet56(pretrained='cifar100')
# resnet_classifier = cifar_resnet56(pretrained='cifar10')

# resnet_modules = list(resnet_classifier.children())[:-1]
# resnet = nn.Sequential(*resnet_modules)

resnet_classifier = nn.DataParallel(resnet_classifier).cuda()

resnet_classifier.eval()
total = 0

test_crossentropy_loss = 0
accuracy_metric = Accuracy()
with torch.no_grad():
    for data, labels_categorical in testloader:
        labels_categorical = labels_categorical.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        total += labels_categorical.size(0)

        pred = resnet_classifier(data)
        # fea = resnet(data).view(-1, 64)
        # print(fea.shape)
        # exit()

        test_crossentropy_loss += F.cross_entropy(pred, labels_categorical, reduction='sum').item()

        accuracy_metric.update((pred, labels_categorical))

test_crossentropy_loss /= total
accuracy = accuracy_metric.compute()
print(f'Crossentropy(reduction sum): {test_crossentropy_loss}, Accuracy({accuracy_metric._type}): {accuracy}')
