import argparse
import datetime
import os
import time

import torch
import torch.nn as nn
import torch.nn.functional as F
from ignite.metrics import Accuracy
from torch.optim import lr_scheduler
from torchvision.models import resnet50

from src.custom_datasets.simpli_dataset import MyDatasetMod
from src.custom_models.lenet5 import LeNet
from src.custom_models.resnet import resnet18
from src.utils import AverageMeter

parser = argparse.ArgumentParser("Transfer Knowledge from resnet 50")

# dataset
parser.add_argument('-d', '--dataset', type=str, default='cifar10',
                    choices=['mnist', 'fashion_MNIST', 'cifar10', 'Caltech101', 'cifar2', 'BioDatasetsLoad',
                             'MyDataset',
                             'WrongDataset'])
parser.add_argument('--path', type=str, default='/home/super/datasets-nas/cifar10')

# optimization
parser.add_argument('--batch-size', type=int, default=128)
parser.add_argument('--lr', type=float, default=0.001, help="learning rate for model")
parser.add_argument('--stepsize', type=int, default=20)
parser.add_argument('--gamma', type=float, default=0.5, help="learning rate decay")

# model
parser.add_argument('--simple-model', type=str, default='resnet18',
                    choices=['lenet', 'resnet18'])
parser.add_argument('--max-epoch', type=int, default=500)
parser.add_argument('--when_save_model', type=int, default='10', help="save model at epoch x")
parser.add_argument('--save-dir', type=str, default='models')
parser.add_argument('--prefix', type=str, default='diff_lern_transfer')

# pythorch seed
parser.add_argument('--seed', type=int, default=1, help='using a fixed seed to be reproducible')

# selected gpus
parser.add_argument('--gpu', type=str, default='2', help='select the gpu or gpus to use')

# data generation
parser.add_argument('--random_gen_size', type=int, default=0, help='random generation data for batch')

args = parser.parse_args()


def main():
    torch.manual_seed(args.seed)
    print(f'random gen size: {args.random_gen_size}')
    test_gpu_settings()

    print(f"Creating dataset: {args.dataset}")

    # dataset = type('obj', (object,), {'num_classes' : 10})
    dataset = MyDatasetMod(args.batch_size, args.path)
    trainloader, testloader, test_labels, global_labels = dataset.trainloader, dataset.testloader, dataset.test_labels, dataset.train_labels

    resnet_classifier = resnet50(pretrained=True)

    resnet_modules = list(resnet_classifier.children())[:-1]
    resnet = nn.Sequential(*resnet_modules)
    resnet = nn.DataParallel(resnet).cuda()

    resnet_classifier = nn.DataParallel(resnet_classifier).cuda()

    if args.simple_model == 'lenet':
        model = LeNet(num_classes=dataset.num_classes)
    elif args.simple_model == 'resnet18':
        model = resnet18(pretrained=False)
    model = nn.DataParallel(model).cuda()

    criterion_mse = nn.MSELoss().cuda()
    criterion_xent = nn.CrossEntropyLoss().cuda()

    params = list(model.parameters())
    optimizer = torch.optim.Adam(params, args.lr)

    scheduler = lr_scheduler.StepLR(optimizer, step_size=args.stepsize, gamma=args.gamma)

    start_time = time.time()
    for epoch in range(args.max_epoch):
        print("==> Start Epoch {}/{}".format(epoch + 1, args.max_epoch))
        train_model(resnet, resnet_classifier, model, trainloader, global_labels, criterion_mse, criterion_xent,
                    optimizer)

        scheduler.step()

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

        test(resnet, model, testloader, test_labels, dataset.num_classes)

        if (epoch + 1) % args.when_save_model == 0:  # Save trained model
            print("Model trained saveing...")
            model_name = f'{args.prefix}_{args.simple_model}_{epoch + 1}(random size: {args.random_gen_size})'
            torch.save(model,
                       f'{args.save_dir}/{model_name}.pt')
        print("==> End Epoch {}/{}".format(epoch + 1, args.max_epoch))


def train_model(resnet, resnet_classifier, model, trainloader, global_labels, criterion_mse, criterion_xent, optimizer):
    model.train()
    mse_losses = AverageMeter()
    fake_mse_losses = AverageMeter()
    xent_losses = AverageMeter()

    for batch_idx, (data, labels) in enumerate(zip(trainloader, global_labels)):
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        # fake data
        if args.random_gen_size != 0:
            fake_data, fake_features = generate_random_data(resnet, data, labels, args.random_gen_size)
            fake_lenet_features, _ = model(fake_data)
            fake_loss_mse = criterion_mse(fake_lenet_features, fake_features)
            optimizer.zero_grad()
            fake_loss_mse.backward()
            optimizer.step()
            fake_mse_losses.update(fake_loss_mse.item(), fake_data.size(0))

        resnet_features = resnet(data)
        resnet_features = resnet_features.view(-1, 2048)

        lenet_features, lenet_predictions = model(data)

        loss_mse = criterion_mse(lenet_features, resnet_features)
        loss_xent = criterion_xent(lenet_predictions, labels)

        # loss = loss_xent
        loss = loss_mse + loss_xent

        # optimizer.zero_grad()
        loss.backward()

        optimizer.step()

        mse_losses.update(loss_mse.item(), labels.size(0))
        xent_losses.update(loss_xent.item(), labels.size(0))

        if (batch_idx + 1) % 80 == 0:
            print(
                f"Batch {batch_idx + 1}\t  MSE {mse_losses.val} ({mse_losses.avg}),  Fake MSE {fake_mse_losses.val} ({mse_losses.avg}), CrossEntropy {xent_losses.val} ({xent_losses.avg})")


def generate_random_data(resnet_extractor, data, labels, size):
    if size == 0:
        return data, labels
    # random generation TODO generalize
    fake_data = torch.rand(size, 3, 32, 32).cuda()
    fake_labels = resnet_extractor(fake_data).view(-1, 2048)

    return fake_data, fake_labels


def test(resnet, model, testloader, test_labels, num_classes):
    model.eval()
    total = 0

    test_mse_loss = 0
    test_crossentropy_loss = 0
    accuracy_metric = Accuracy()
    with torch.no_grad():
        for data, labels_categorical in zip(testloader, test_labels):
            labels_categorical = labels_categorical.cuda(non_blocking=True)
            data = data.cuda(non_blocking=True)

            total += labels_categorical.size(0)

            resnet_features = resnet(data)
            resnet_features = resnet_features.view(-1, 2048)
            lenet_features, lenet_predictions = model(data)

            test_mse_loss += F.mse_loss(lenet_features, resnet_features, reduction='sum').item()
            test_crossentropy_loss += F.cross_entropy(lenet_predictions, labels_categorical, reduction='sum').item()

            # pred_categorical = lenet_predictions.argmax(dim=1, keepdim=True).reshape(-1)
            # pred_one_hot = torch.zeros(len(pred_categorical), num_classes).cuda().scatter_(1,
            #                                                                                pred_categorical.unsqueeze(
            #                                                                                    1), 1.)
            # labels_one_hot = torch.zeros(len(labels_categorical), num_classes).cuda().scatter_(1,
            #                                                                                    labels_categorical.unsqueeze(
            #                                                                                        1), 1.)
            accuracy_metric.update((lenet_predictions, labels_categorical))

    test_mse_loss /= total
    test_crossentropy_loss /= total
    accuracy = accuracy_metric.compute()
    print(
        f'Test MSE(reduction sum): {test_mse_loss}, Crossentropy(reduction sum): {test_crossentropy_loss}, Accuracy({accuracy_metric._type}): {accuracy}')


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    # sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
    #     args.percentage_used) + '.txt'))
    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        print(f"Learning rate optimizer {args.lr}  Batch-size {args.batch_size}")
    else:
        print("Currently using CPU")
        exit()


if __name__ == '__main__':
    main()
