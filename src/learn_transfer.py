import argparse
import datetime
import os
import time

import torch
import torch.nn as nn
import torch.nn.functional as F
from ignite.metrics import Accuracy
from torch.optim import lr_scheduler

import cifar10_models
from src.custom_datasets.read_dataset_torch import get_dataset_loaders
from src.custom_models import lenet_paper
from src.custom_models.alexnet import AlexNet
from src.custom_models.lenet5 import LeNet
from src.custom_models.mobile_ne_v2 import mobilenet_v2
from src.custom_models.resnet import resnet18
from src.custom_models.resnet_cifar100 import cifar_resnet56, cifar_resnet44, cifar_resnet20
from src.utils import AverageMeter

parser = argparse.ArgumentParser("Transfer Knowledge from resnet 50")

# dataset
parser.add_argument('-d', '--dataset', type=str, default='cifar10',
                    choices=['mnist', 'fashion_MNIST', 'cifar10', 'Caltech101', 'cifar2', 'BioDatasetsLoad',
                             'MyDataset',
                             'WrongDataset'])
parser.add_argument('--path', type=str, default='/home/supreme/datasets/cifar10')

# optimization
parser.add_argument('--batch-size', type=int, default=128)
parser.add_argument('--lr', type=float, default=0.001, help="learning rate for model")
parser.add_argument('--stepsize', type=int, default=50)
parser.add_argument('--gamma', type=float, default=0.1, help="learning rate decay")

# model
parser.add_argument('--teacher', type=str, default='resnet18',
                    choices=['resnet20', 'resnet44', 'resnet56', 'resnet18', 'resnet50'])
parser.add_argument('--simple-model', type=str, default='lenet',
                    choices=['lenet', 'resnet18', 'mobilenetV2', 'alexnet', 'lenet_paper'])
parser.add_argument('--max-epoch', type=int, default=200)
parser.add_argument('--when_save_model', type=int, default='10', help="save model at epoch x")
parser.add_argument('--save-dir', type=str, default='models')

# pythorch seed
parser.add_argument('--seed', type=int, default=1, help='using a fixed seed to be reproducible')

# selected gpus
parser.add_argument('--gpu', type=str, default='0', help='select the gpu or gpus to use')

# data generation
parser.add_argument('--random_gen_size', type=int, default=0, help='random generation data for batch')

args = parser.parse_args()


def main():
    torch.manual_seed(args.seed)

    print('--- START Arg parse ---')
    options = vars(args)
    for k, v in options.items():
        print(k, ':', v)
    print('--- END Arg parse ---')

    test_gpu_settings()

    trainloader, testloader, num_classes = get_dataset_loaders(args.path, args.batch_size)

    if args.teacher == 'resnet50':
        resnet_classifier = cifar10_models.resnet50(pretrained=True)
        features_size = 2048
    elif args.teacher == 'resnet18':
        resnet_classifier = cifar10_models.resnet18(pretrained=True)
        features_size = 512
    elif args.teacher == 'resnet20':
        resnet_classifier = cifar_resnet20(pretrained=args.dataset)
        features_size = 64
    elif args.teacher == 'resnet44':
        resnet_classifier = cifar_resnet44(pretrained=args.dataset)
        features_size = 64
    elif args.teacher == 'resnet56':
        resnet_classifier = cifar_resnet56(pretrained=args.dataset)
        features_size = 64

    resnet_modules = list(resnet_classifier.children())[:-1]
    resnet = nn.Sequential(*resnet_modules)
    resnet = nn.DataParallel(resnet).cuda()

    resnet_classifier = nn.DataParallel(resnet_classifier).cuda()

    model = create_model(args.simple_model, features_size, num_classes)
    model = nn.DataParallel(model).cuda()

    criterion_mse = nn.MSELoss().cuda()
    criterion_xent = nn.CrossEntropyLoss().cuda()

    params = list(model.parameters())
    optimizer = torch.optim.Adam(params, args.lr)

    scheduler = lr_scheduler.StepLR(optimizer, step_size=args.stepsize, gamma=args.gamma)

    start_time = time.time()
    for epoch in range(args.max_epoch):
        print("==> Start Epoch {}/{}".format(epoch + 1, args.max_epoch))
        train_model(resnet, resnet_classifier, model, trainloader, criterion_mse, criterion_xent,
                    optimizer, features_size)

        scheduler.step()

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

        test(resnet, model, testloader, num_classes, features_size)

        if (epoch + 1) % args.when_save_model == 0:  # Save trained model
            print("Model trained saveing...")
            torch.save(model,
                       f'{args.save_dir}/{args.simple_model}_{epoch + 1}(random size: {args.random_gen_size}).pt')
        print("==> End Epoch {}/{}".format(epoch + 1, args.max_epoch))


def create_model(model_name, features_size, num_classes):
    if model_name == 'lenet':
        model = LeNet(num_classes=num_classes, features_size=features_size)
    elif model_name == 'lenet_paper':
        model = lenet_paper.LeNet(num_classes=num_classes, features_size=features_size)
    elif model_name == 'resnet18':
        model = resnet18(pretrained=False, **{'num_classes': num_classes, 'features_size': features_size})
    elif model_name == 'mobilenetV2':
        model = mobilenet_v2(pretrained=False, **{'num_classes': num_classes, 'features_size': features_size})
    elif model_name == 'alexnet':
        model = AlexNet(num_classes=num_classes, features_size=features_size)
    elif model_name == 'resnet50':
        model = cifar10_models.resnet50(pretrained=False, **{'num_classes': num_classes})
    return model


def train_model(resnet, resnet_classifier, model, trainloader, criterion_mse, criterion_xent, optimizer, features_size):
    model.train()
    mse_losses = AverageMeter()
    xent_losses = AverageMeter()

    for batch_idx, (data, labels) in enumerate(trainloader):
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        data, labels = generate_random_data(resnet_classifier, data, labels, args.random_gen_size)
        # print(data.shape)
        # print(labels.shape)

        resnet_features = resnet(data)
        resnet_features = resnet_features.view(-1, features_size)

        lenet_features, lenet_predictions = model(data)

        loss_mse = criterion_mse(lenet_features, resnet_features)
        loss_xent = criterion_xent(lenet_predictions, labels)

        # loss = loss_xent
        loss = loss_mse + loss_xent

        optimizer.zero_grad()
        loss.backward()

        optimizer.step()

        mse_losses.update(loss_mse.item(), labels.size(0))
        xent_losses.update(loss_xent.item(), labels.size(0))

        if (batch_idx + 1) % 80 == 0:
            print(
                f"Batch {batch_idx + 1}\t  MSE {mse_losses.val} ({mse_losses.avg}), CrossEntropy {xent_losses.val} ({xent_losses.avg})")


def generate_random_data(resnet_classifier, data, labels, size):
    if size == 0:
        return data, labels
    # random generation TODO generalize
    fake_data = torch.rand(size, 3, 32, 32).cuda()
    fake_labels = resnet_classifier(fake_data).argmax(dim=1, keepdim=True).reshape(-1).cuda()
    # print(fake_data.shape)
    # print(fake_labels.shape)
    concat_data = torch.cat((data, fake_data), 0)
    concat_labels = torch.cat((labels, fake_labels))
    # print(concat_data.shape)
    # print(concat_labels.shape)
    return concat_data, concat_labels


def test(resnet, model, testloader, num_classes, features_size):
    model.eval()
    total = 0

    test_mse_loss = 0
    test_crossentropy_loss = 0
    accuracy_metric = Accuracy()
    with torch.no_grad():
        for data, labels_categorical in testloader:
            labels_categorical = labels_categorical.cuda(non_blocking=True)
            data = data.cuda(non_blocking=True)

            total += labels_categorical.size(0)

            resnet_features = resnet(data)
            resnet_features = resnet_features.view(-1, features_size)
            lenet_features, lenet_predictions = model(data)

            test_mse_loss += F.mse_loss(lenet_features, resnet_features, reduction='sum').item()
            test_crossentropy_loss += F.cross_entropy(lenet_predictions, labels_categorical, reduction='sum').item()

            accuracy_metric.update((lenet_predictions, labels_categorical))

    test_mse_loss /= total
    test_crossentropy_loss /= total
    accuracy = accuracy_metric.compute()
    print(
        f'Test MSE(reduction sum): {test_mse_loss}, Crossentropy(reduction sum): {test_crossentropy_loss}, Accuracy({accuracy_metric._type}): {accuracy}')


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    # sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
    #     args.percentage_used) + '.txt'))
    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        print(f"Learning rate optimizer {args.lr}  Batch-size {args.batch_size}")
    else:
        print("Currently using CPU")
        exit()


if __name__ == '__main__':
    main()
