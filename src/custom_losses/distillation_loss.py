import torch
from torch import nn
import torch.nn.functional as F


class DistillationLoss(torch.nn.Module):
    def __init__(self):
        super(DistillationLoss, self).__init__()
        self.alpha = torch.tensor(1.0)
        # self.alpha = torch.nn.Parameter(self.alpha)
        self.T = torch.tensor(1.0)
        # self.T = torch.nn.Parameter(self.T)

    def forward(self, student_outputs, teacher_outputs):
        upper_softmax = F.log_softmax(student_outputs / self.T, dim=1)
        downer_softmax = F.softmax(teacher_outputs / self.T, dim=1)
        KD_loss = nn.KLDivLoss()(upper_softmax,
                                 downer_softmax) * (self.T * self.T)
        return KD_loss
        # student_softmax = F.softmax(student_outputs, dim=1)
        # teacher_softmax = F.softmax(student_outputs, dim=1)
        # knowledge_distillation_loss = 1 / (alpha * T * T) * (teacher_softmax - student_softmax).sum()
        # return knowledge_distillation_loss


if __name__ == '__main__':
    dist_loss = DistillationLoss(t=1)

    student_output = torch.tensor([
        [.2, .8],
        [1., 0.]
    ]).cpu()
    teacher_output = torch.tensor([
        [.2, .8],
        [.7, .3]
    ]).cpu()

    loss = dist_loss(student_output, teacher_output)
    print(loss)
    # loss.backward()
