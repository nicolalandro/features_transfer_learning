# Transfer learning using features

# Install requirements
```bash
pip install -r requirements.txt
CUDA_HOME=/usr/local/cuda/ pip install git+https://github.com/mapillary/inplace_abn.git@v1.0.11  
```

## run
```bash
python3 src/learn_transfer.py --simple-model lenet --teacher resnet56
python3 src/no_transfer_learning.py --simple-model lenet
```

```bash
nohup python3.7 src/learn_transfer.py --gpu 1 --lr 0.01 --stepsize 60 --gamma 0.1 --simple-model lenet_paper --teacher resnet56 --random_gen_size 50 > logs/lenet_paper_aug_50v3.log 2>&1 &!
```

```bash
nohup python3.7 src/ntsnet_learn_transfer.py --paht '/home/supreme/datasets/CUB_200_2011' --features-path '/home/supreme/datasets/CUB_200_2011/features' --gpu 0 --lr 0.01 --stepsize 500 --simple-model mobilenetV2 --teacher none > logs/mobilenetv2_aug_0v3.log 2>&1 &!
```

# Fine grane network
* nts-net
* BilinearCNN
* [tasn](https://github.com/researchmm/tasn)
* [TBMSL-](https://github.com/ZF1044404254/TBMSL-Net)
* [Tafe-Net](https://github.com/ucbdrive/tafe-)
* [TResNet](https://github.com/mrT23/)

# Knowledge Distillation like Hinton
* Code from [source](https://github.com/peterliht/knowledge-distillation-pytorch/blob/master/model/net.py):
```
def loss_fn_kd(outputs, labels, teacher_outputs, params):
    """
    Compute the knowledge-distillation (KD) loss given outputs, labels.
    "Hyperparameters": temperature and alpha
    NOTE: the KL Divergence for PyTorch comparing the softmaxs of teacher
    and student expects the input tensor to be log probabilities! See Issue #2
    """
    alpha = params.alpha
    T = params.temperature
    KD_loss = nn.KLDivLoss()(F.log_softmax(outputs/T, dim=1),
                             F.softmax(teacher_outputs/T, dim=1)) * (alpha * T * T) + \
              F.cross_entropy(outputs, labels) * (1. - alpha)

    return KD_loss
```
* Romero alternative [FitNet](https://github.com/adri-romsor/FitNets)


# Citation
```bibtex
@article{landro2020can,
  title={Can a powerful neural network be a teacher for a weaker neural network?},
  author={Landro, Nicola and Gallo, Ignazio and La Grassa, Riccardo},
  journal={arXiv preprint arXiv:2005.00393},
  year={2020}
}
```